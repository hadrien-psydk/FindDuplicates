FindDuplicates
https://gitlab.com/pdea/FindDuplicates

Logiciel sous license GPL v3
https://www.gnu.org/licenses/quick-guide-gplv3.html

*****************************
What it does (summary) :
*****************************
Help you find and delete duplicates in one or more directories.
A duplicate consist in 2 files with same checksum (adler32sum) and same size, but potentially different name and/or path.
No file is deleted without confirmation!
The advantage of this sofware is to propose mass operations (filter, delete) on found duplicates to help you tidy up your disk, not one file by one file.

*****************************
How to launch :
*****************************
python FindDuplicatesQtGui.py
Python version must be python 3 with PyQt installed (not python 2)
See Minimal configuration below

Or with optional parameters :
python FindDuplicatesQtGui.py -c ConfigFile.xml -d DirectoryToScan -s SelectionPattern -e ExcludePattern

*****************************
Little story :
*****************************
You sometimes copy files from one computer to another, or to a USB key. 
You also perform copy of important files.
But after a moment, you are a bit lost, because you have a lot of duplicates of same files 
and its hard to find because you can change file name or file organisation in folders.
And you fear to delete files because you are not sure it is the exact same file or newer version of this file.
Then this little software is for you. :-)


*****************************
What it does (long version) :
*****************************
After scanning specified directory(ies), this software will display an interface to allow you to choose which file of each duplicate you want to delete.

You can sort result by file size, file 1 or 2 path or file name. Reverse sort if you click twice by same sort button.
It is also possible to sort by selection after you started to select files, in order to have selected files at the beginnning.

Selection : You can select file 1 or 2. 1 click on column 3 to select file 1, 1 click on column 4 to select file 2.
Depending on "Allow double select" option, you can select or not both files.

You can select files by pattern in FilePath or in FileName. 
You can select by anti-pattern (see help) in Path.

Selected files can be wether:
- excluded (not considered as duplicates), 
- or deleted (with confirmation) 
- or unselected. ;-)

*** HINT ***
If you Double-Click in first column on a duplicate, you will get directory 1 path in clipboard. On second column, you will get file 1 full path.
On 5th column, you will get directory 2 path. On 6th column, you will get file 2 full path.
This, to open directory in explorer or file with external editor or tool. 
But also to help you create special pattern by copying it in 'Pattern' input zone to search and select files.

This software brings you config file and command line functionalities.

See help for more details.


*****************************
Minimal configuration :
*****************************
From sources, you need python 3 to launch this program, but also PyQt5 python lib:
	python.exe -m pip install --upgrade pip
    python.exe -m pip install PyQt5

windows compiled version is stand alone.
To compile Windows binary, you also need cx_Freeze library
    python.exe -m pip install cx_Freeze --upgrade
	python.exe Builder.py build
    or Build.bat

*****************************
Help :
*****************************
- Double-click on FilePath 1 or 2 column (1 or 5) will copy directory path in clipboard.
Double-click on fileName 1 or 2 column (2 or 6) will copy full file path in clipboard.


- When there is only one directory scanned, file Paths are displayed relatively to this base directory (shorter).
The path is empty if the file is at the root of scan directory.
You can resize list column width.


- Exclude a duplicate consist in excluding it from list of duplicates, so it will no more be considered as a duplicate. It will *NOT* be deleted so.
Do this with directory containing safeguard copies of your files. Or copies you know as duplicates but which you need on both directories.
Exclude is the exact opposite of delete !


- Pattern : a pattern is a directory ("/home/me/old/") or a part of a directory ("/old/") or of a file name ("_svg", ".old", "_old.").
You can use Pattern to select (pattern) or not-select (anti-pattern = select the other file in duplicate pair).
And then to exclude or delete files.

Description of anti-pattern ("safe" directory for example) selection behaviour. 
With anti-pattern, pattern is the criteria for *NOT* selecting. 
If only Path1 correspond to pattern, select File 2. The same for Path 2. If both correspond to Pattern, select none.
If none of Path 1 and 2 correspond to pattern, select none. Pattern must be present on one file to select the other file.
Example of usage : You have a directory you know to be the one you want to keep. 
All duplicates not in this directory shall be selected to be deleted (after check and confirmation of course!).


- Allow double select : activate this option to be able to select both file1 and file2 of a duplicate so you can delete both at the same time.
CAUTION : This option is then dangerous because it permits you to completely delete a file and its copy if there is only one duplicate.


- Config file 
When you add:
- a directory to scan, 
- or a pattern in path or fileName to select, 
- or a pattern in path to exclude, 
a configuration file is prepared for you.
You can save it if you want, for next time you want to analyse same directory(ies). You will then be able to load it.
Since it is saved as xml file, you can edit it with a text editor.

You can replace in xml config file "PatternInFilePathToSelect" by "PatternInFileNameToSelect" 
if you want to select files by pattern in their name instead of by their path.

You can also replace in xml config file "PatternInFilePathToSelect" by "PatternToExclude"
if you want to exclude directly files without the step of selection to verify.


- Note : If 3 files are duplicates, it will be displayed twice on two separate lines.
One line with file 1 and file 2. Another line with file 2 and file 3.
If a pattern detects file 2 and 3 to select:
- when comparing file 2 and 3 (with option "Allow double select" off), they are not selected because both correspond to the pattern,
- when comparing file 1 and 2, file 2 is selected.
So when duplicates are displayed, file 2 will be selected on both lines because it is the same file.
CAUTION: That mean you sometime have to search and select again for duplicates to delete each duplicate (cf. file 3 here).


- Difference between "Scan directory" and "Add directory to scan" : 
"Scan directory" will clear previous results before scanning new directory.
"Add directory to scan" will add directory scan result to previous scan result and find duplicates on total scan result.


- command line :
python FindDuplicates -c ConfigFile.xml -d DirectoryToScan -s SelectionPattern -e ExcludePattern
You can provide multiple Directory to scan, multiple selection pattern and multiple exclude pattern.
You shall not use both -c and { -d, -s, -e }
python FindDuplicates -h display help message


- If duplicates (only duplicates) have path or fileName with encoding errors, 
they will be displayed on standard output and on a file "ErrFileEncoding.csv".
These files will be automatically excluded from duplicate list.


*****************************
Acknowledgements :
*****************************
Thanks to Erwan L.P.  to convinced me to learn Python and other programming languages ! ;-)
Thanks to Jérôme A.G. to convinced me to learn Perl also (which has been useful for me in the past) ! :-D

Developed by Florent PICHOT in 2017 and 2020-2021 (Covid effect!)
Please make a donation if this software was useful for you, to show your gratitude. :-)


*****************************
Releases :
*****************************
- v2.0 10/04/2021 GUI completely recreated with PyQt (bloody hard work !)
   Please be kind, it is my first development with PyQt !
   Huge improvment of performances for disctories with a lot of files
   Ideas of improvements: 
   Display directories with no file in it to delete empty directories. Display empty files (size=0)

- v1.0 30/10/2017 First release for usage, but also for bug reporting, comments and proposal of improvement.
   Please be kind, its my first development with wxPython.
   Known bug : Pop-up windows doesn't display correctly (grey, no message displayed) sometimes the second time !?!
   Ideas of improvements: Display list of files with same names but different size/checksum (in order to rename one file and merge directories)
