#include "fdup_guimain.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FDup_GuiMain w;
    w.show();

    return a.exec();
}
