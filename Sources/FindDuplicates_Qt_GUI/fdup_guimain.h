#ifndef FDUP_GUIMAIN_H
#define FDUP_GUIMAIN_H

#include <QMainWindow>

namespace Ui {
class FDup_GuiMain;
}

class FDup_GuiMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit FDup_GuiMain(QWidget *parent = 0);
    ~FDup_GuiMain();

private:
    Ui::FDup_GuiMain *ui;
};

#endif // FDUP_GUIMAIN_H
