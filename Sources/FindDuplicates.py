#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on August the 6th, 2017

@author: florent Pichot
'''

from   operator import attrgetter
import time

import os.path
from   os       import walk
import zlib
from threading import Thread, RLock
#import Instrumentor

# structure FileWithInfo (path, filename, size, checksum)
class FileWithInfo:
    def __init__(self, pFileName, pPath, pSize, pCheckSum, pIsSelecteded):
        self.FileName   = pFileName
        self.Path       = pPath
        self.Size       = pSize
        self.CheckSum   = pCheckSum
        self.IsSelected = pIsSelecteded
    def __repr__(self):
        return repr((self.FileName, self.Path, self.Size, self.CheckSum, self.IsSelected))
    def setIsSelecteded(self, pIsSelecteded):
        self.IsSelected = pIsSelecteded
    def getIsSelected(self):
        return self.IsSelected

# structure DuplicateFileWithInfo : file1 and file2 info (path, filename, size, checksum)
class DuplicateFileWithInfo:
    def __init__(self, File1, File2):
        self.File1 = File1
        self.File2 = File2
    def __repr__(self):
        return repr((self.File1, self.File2))

class CMyThread(Thread):
    def __init__(self, parent, pId, pNbOfThreads):
        self.parent      = parent
        self.id          = pId
        self.nbOfThreads = pNbOfThreads
        Thread.__init__(self)
        
    def run(self):
        hasToExit       = False # hasToExit only in case of OSErrors because too much file open for system
        keepFromExiting = False
        # repeat until must exit
        #until( ( (    self.parent.hasToStop() ) or  (     hasToExit ) ) and ( !keepFromExiting ) ):
        while( ( ( not self.parent.hasToStop() ) and ( not hasToExit ) ) or ( keepFromExiting ) ):
            keepFromExiting = False
            # checkIf SomethingToDo = take MutexParam, check if not empty, if not, take parameter, free mutex
            somethingToDo = False
            retrievedParams = []
            listParamsLen = self.parent.getInputSize()
            # Check if something to do
            if( listParamsLen != 0 ):
                retrievedParams = self.parent.takeOneInput()
                somethingToDo = not ( retrievedParams is None )
                #startTime = time.time() 
                #print( str(self.id) + "- " + str(listParamsLen))
            
            # if something to do : do it
            if( somethingToDo ):
                #localData = Instrumentor.InstrumentationTimer( "Thread2HasSThgToDo;fi;" + str( retrievedParams[3] ), 100 + self.id )
                lFileSize = retrievedParams[2]
                # call method 1
                #print( "Params : <" + retrievedParams[0] + "> - <" + retrievedParams[1] + ">" )
                try:
                    lCheckSum = self.parent.computeOneChecksum( retrievedParams[0], retrievedParams[1] )
                    # save fileSize and checkSum
                    self.parent.updateInListOfFiles( retrievedParams[3], lCheckSum, lFileSize )
                        #print("ProcessTime ;" + str(time.time() - startTime) + " ;for file of size ;" + str( lFileSize ) )
                        #print( str(self.id) + "+ " + str(listParamsLen) + " - " + str( retrievedParams[3] ) )
                except OSError: # as error:
                    # Too many opened files (? or same file openned twice ?)
                    # exit from thread if more than half nb of programmed threads
                    #hasToExit = self.parent.shallExit() #check if shall reduce nb of active threads
                    #print("Too manu opened files ? " + str(error) + " => try again later")
                    self.parent.reintroduceOneInput( self.id, retrievedParams[0], retrievedParams[1], retrievedParams[2], retrievedParams[3] )
                    keepFromExiting = True #keep alive to be sure someone can take this new element
            hasToExit = ( self.parent.getInputSize() == 0 )
        self.parent.closeOneThread( self.id )


class MultipleThreads:
    def __init__(self, nbOfThreads, pListOfFiles, pComputeOneChecksum, pUpdateChecksumInListOfFiles, pProgressIndicator):
        self.nbOfThreads               = nbOfThreads
        self.nbOfActualThreads         = nbOfThreads
        self.parentComputeOneChecksum  = pComputeOneChecksum
        self.parentUpdateInListOfFiles = pUpdateChecksumInListOfFiles
        self.listOfFiles               = pListOfFiles
        self.progressIndicator         = pProgressIndicator
        self.sumOfFileSize             = 0 #pSumOfFileSize
        self.totalAnalysedFileSize     = 0
        self.priorityToLoading         = True
        self.threadMustExit            = False
        self.listOfFileToCompute       = []
        self.mutexParam                = RLock() #to protect use of listOfFileToCompute
        self.mutexResult               = RLock() # to protect call to parentUpdateInListOfFiles
        self.listOfThreads = []

    def shallExit( self ):
        return( self.nbOfActualThreads > ( self.nbOfThreads / 2 ) )
    
    def closeOneThread( self, Id ):
        #localData = Instrumentor.InstrumentationTimer( "closeOneThread", Id )
        self.nbOfActualThreads -= 1
        #print("Close thread " + str(Id) + " remains " + str(self.nbOfActualThreads) )
    
    def hasToStop( self ):
        return( self.threadMustExit )

    def launchOneChecksumCompute( self, pIndex, pDirPath, pFileName, pFileSize ):
        #localData = Instrumentor.InstrumentationTimer("AddOneComputeCheckSum", 10)
        # Take mutexParam, 
        with self.mutexParam:
            # Add params in listOfFileToCompute
            self.listOfFileToCompute.append( [ pDirPath, pFileName, pFileSize, pIndex ] )
        # free mutexParam
        self.sumOfFileSize += pFileSize
        self.progressIndicator.setMax( self.sumOfFileSize )  #print("+ " + str( inputListLen ))

    def waitAllThreadHaveFinished( self ):
        #localData = Instrumentor.InstrumentationTimer("waitAllThreadHaveFinished", 2)
        self.nbOfActualThreads         = min( self.nbOfThreads, len( self.listOfFileToCompute ) )
        #print( "waitAllThreadHaveFinished not started=" + str( len( self.listOfFileToCompute ) ) + " active threads=" + str( self.nbOfActualThreads ) + " AnalysedFileSize=" + fileSizeToStr( self.totalAnalysedFileSize ) + " totalFileSize=" + fileSizeToStr( self.sumOfFileSize ) )
        
        #start all threads
        for i in range(0,self.nbOfActualThreads):
            lThread = CMyThread(self, i, self.nbOfActualThreads)
            lThread.daemon = True
            self.listOfThreads.append( lThread )
        for oneThread in self.listOfThreads:
            oneThread.start()         
        self.priorityToLoading = False # unleach threads       

        # wait all files have been treated
        while( len( self.listOfFileToCompute ) != 0 ):
            #print( "Still " + str( len( self.listOfFileToCompute ) ) + " files not started to process. active threads=" + str( self.nbOfActualThreads )) # debug trace to comment
            time.sleep(10.0)
        # ask thread to exit
        self.threadMustExit = True
        time.sleep(0.5)
        #print("Wait all threads have finished processing")
        while( self.nbOfActualThreads != 0 ):
            time.sleep(10.0)
            #print( "Still " + str( self.nbOfActualThreads ) + " threads working" ) # debug trace to comment
        # wait all threads have finished
        for oneThread in self.listOfThreads:
            oneThread.join()

    # this function don't need protection
    def computeOneChecksum( self, pDirPath, pFileName ):
        return( self.parentComputeOneChecksum( pDirPath, pFileName ) )
    
    def getInputSize( self ):
        lenListParam = len( self.listOfFileToCompute )
        #print( " Remains " + str(lenListParam) + " file to scan" )
        return( lenListParam )
    
    def takeOneInput( self ):
        with self.mutexParam:
            if( len(self.listOfFileToCompute) == 0 ):
                return None
            return( self.listOfFileToCompute.pop() )
    
    #self.parent.reintroduceOneInput( retrievedParams[0], retrievedParams[1], retrievedParams[2], retrievedParams[3] )
    def reintroduceOneInput( self, pThreadId, pElt1, pElt2, pElt3, pElt4 ):
        with self.mutexParam:
            #localData2 = Instrumentor.InstrumentationTimer( "reintroduce;fi;" + str( pElt4 ) + ";file;" +  pElt1 + "\\" + pElt2 , 100 + pThreadId )
            self.listOfFileToCompute.append( [ pElt1, pElt2, pElt3, pElt4 ] )
            #print( "Thread " + str( pThreadId ) + " return one file fi=" + str( pElt4 ) + " file " +  pElt1 + "\\" + pElt2 )
            
    # this function call shall be protected with self.mutexResult
    def updateInListOfFiles( self, pIndex, pCheckSum, pFileSize ):
        with self.mutexResult:
            #self.nbOfFilesFound += 1
            self.totalAnalysedFileSize += pFileSize
            self.progressIndicator.setCurrent( self.totalAnalysedFileSize )
            if( pFileSize != 0 ):
                #Do not add empty files
                self.parentUpdateInListOfFiles( self.listOfFiles, pIndex, pCheckSum )
                #self.sumOfFileSize += pFileSize

# compute adler32sum of filename
def adler32sum(pFilename, pBlocksize=65536):
    checksum = 0
    with open(pFilename, "rb") as f:
        for block in iter(lambda: f.read(pBlocksize), b""):
            checksum = zlib.adler32(block, checksum)
    return checksum & 0xffffffff


# clear content of a list by deleting each element of it
def clearList(pOneList):
    lIndex=len(pOneList)-1
    while ( lIndex >= 0 ):
        del pOneList[lIndex]
        lIndex-=1

# get a string representing a number with no more than 3 or 4 digits ("1250", "650", "15.6", "9.56")
# pSize shall be between 2.00 to 1999.99
def getFixedDigitNumberAsString( pSize ):
    lReturnString = "0"
    if( pSize != 0 ):
        lLength = len(str(int(pSize))) #size of integer part of the number
        if( lLength >= 3):
            lReturnString = "{:.0f}".format(pSize) # no digit after '.' ((1250( or "650")
        elif( lLength == 2 ):
            lReturnString = "{:.1f}".format(pSize) # 2 digits before and 1 digit after '.' (15.6)
        else:
            lReturnString = "{:.2f}".format(pSize) # 1 digit before and 2 digits after '.' ("9.56")
    return lReturnString

# function to transform file(s) sizes to string
# "xx.x To", "xx.x Go", "xx.x Mo", "xx.x ko" or "xx.x oct"
def fileSizeToStr(pFileSize):
    lReturnedString = ""
    lSizeLimit      = 2*1024 # 2 ko
    if ( pFileSize < lSizeLimit ):
        #lReturnedString = "{:.3f}".format(pFileSize) + " oct"
        lReturnedString = getFixedDigitNumberAsString( pFileSize) + " oct"
    else:
        lSizeLimit *= 1024 # 2 Mo 
        if ( pFileSize < lSizeLimit ):
            #lReturnedString = "{:.3f}".format(pFileSize/1024) + " ko"
            lReturnedString = getFixedDigitNumberAsString( pFileSize/1024 ) + " ko"
        else:
            lSizeLimit *= 1024 # 2 Go 
            if ( pFileSize < lSizeLimit ):
                #lReturnedString = "{:.3f}".format(pFileSize/1024/1024) + " Mo"
                lReturnedString = getFixedDigitNumberAsString( pFileSize/1024/1024 ) + " Mo"
            else:
                lSizeLimit *= 1024 # 2 To 
                if ( pFileSize < lSizeLimit ):
                    #lReturnedString = "{:.3f}".format(pFileSize/1024/1024/1024) + " Go"
                    lReturnedString = getFixedDigitNumberAsString( pFileSize/1024/1024/1024 ) + " Go"
                else:
                    #lReturnedString = "{:.3f}".format(pFileSize/1024/1024/1024/1024) + " To"
                    lReturnedString = getFixedDigitNumberAsString( pFileSize/1024/1024/1024/1024 ) + " To"
    #print("fileSizeToStr(" + str(fileSize) + ")=" + lReturnedString)
    return lReturnedString


def scanOneFile(pDirPath, pFileName):
    lFullPath = os.path.join(pDirPath, pFileName)
    lFileSize = os.path.getsize(lFullPath)
    lCheckSum = None # adler32sum(lFullPath) compute adler32sum only when needed (for two files of same size)
    return( lFileSize, lCheckSum )

def computeChecksumOfOneFile(pDirPath, pFileName):
    lFullPath = os.path.join(pDirPath, pFileName)
    lCheckSum = adler32sum(lFullPath)
    return( lCheckSum )

def addInListOfFiles(pDirPath, pFileName, pFileSize, pCheckSum, pListOfFiles):
    pListOfFiles.append(FileWithInfo(pFileName, pDirPath, pFileSize, pCheckSum, False))
    return(pFileSize)


def updateChecksumInListOfFiles( pListOfFiles, pIndex, pChecksum ):
    pListOfFiles[pIndex].CheckSum = pChecksum

# scan all file in pOneDirectory and its sub directory to compute file CRC and file size, store these info in pListOfFiles
def scanDirectory(pOneDirectory, pListOfFiles, pPreviousTotalNbOfFileScanned, pProgressIndicator):
    if ( os.path.isdir(pOneDirectory) ):
        print(pOneDirectory + " found")
        lStartTime      = time.time()
        pProgressIndicator.setTitle("Compute file sizes")
        pProgressIndicator.setMode( 1 )
        lNbOfFilesFound = len(pListOfFiles) # start from nb of files of previous directories scanned
        lSumOfFileSizes = pPreviousTotalNbOfFileScanned # idem

        for (dirpath, dirnames, filenames) in walk(pOneDirectory):
            for OneFile in filenames :
                fileSize, emptyCheckSum = scanOneFile( dirpath, OneFile )
                lNbOfFilesFound += 1
                lSumOfFileSizes += fileSize
                pProgressIndicator.setCurrent( lNbOfFilesFound )
                addInListOfFiles( dirpath, OneFile, fileSize, emptyCheckSum, pListOfFiles )
        print("--> Found "+ str(lNbOfFilesFound)+ " files, for a total of " + fileSizeToStr(lSumOfFileSizes) + " scanned in "+str(time.time()-lStartTime)+" seconds")        
    else:
        print(pOneDirectory + " not found")
    return(lNbOfFilesFound, lSumOfFileSizes)

def countNbOfFiles( pOneDirectory, pProgressIndicator ):
    nbOfFiles = 0
    if ( os.path.isdir(pOneDirectory) ):
        try:
            for (dirpath, dirnames, filenames) in walk(pOneDirectory):
                for OneFile in filenames :
                    nbOfFiles += 1
                    pProgressIndicator.setMax( nbOfFiles )
        except:
            print("Failed to scan " + pOneDirectory)
    return(nbOfFiles)

# find duplicated in a list of files pListOfFiles and return (lListDuplicates, lNumberOfDuplicate, lSumOfDuplicateSize)
def findDuplicates(pListOfFiles, pSumOfFileSize, pProgressIndicator):
    pListOfFiles.sort(key=attrgetter("Size","FileName","Path"), reverse=True)
    #print(pListOfFiles)
    lNumberOfDuplicate  = 0
    lListDuplicates     = list()
    pProgressIndicator.setTitle("Compute CheckSum")
    pProgressIndicator.setMode( 2 )
    pProgressIndicator.setMax(pSumOfFileSize)
    pProgressIndicator.setCurrent( 0 )
    lLastCheckSum       = -1
    lLastFileSize       = -1
    lSumOfDuplicateSize = 0
    lNbOfEmptyFiles     = 0
    lNbOfCheckSumToCompute = 0
    myMultipleThreads = MultipleThreads( 8, pListOfFiles, computeChecksumOfOneFile, updateChecksumInListOfFiles, pProgressIndicator)
    for i,eachFileInfo in enumerate(pListOfFiles):
        if ( i != 0 ):
            # eliminate empty files and case where both files are same file (!?!)
            if ( ( eachFileInfo.Size != 0 ) and ( not ( (pListOfFiles[i-1].Path+"/"+pListOfFiles[i-1].FileName) == (eachFileInfo.Path+"/"+eachFileInfo.FileName) ) ) ):
                if ( eachFileInfo.Size == lLastFileSize ):
                    # if checkSum not yet computed, compute them
                    if( lLastCheckSum is None ):
                        lNbOfCheckSumToCompute += 1
                        myMultipleThreads.launchOneChecksumCompute( i-1, pListOfFiles[i-1].Path, pListOfFiles[i-1].FileName, pListOfFiles[i-1].Size )
                    if( eachFileInfo.CheckSum is None ):
                        lNbOfCheckSumToCompute += 1
                        myMultipleThreads.launchOneChecksumCompute( i, eachFileInfo.Path, eachFileInfo.FileName, eachFileInfo.Size )
                        eachFileInfo.CheckSum = -1
            else:
                if ( eachFileInfo.Size == 0 ):
                    lNbOfEmptyFiles += 1
                    #print("\nFound empty file : " + eachFileInfo.Path + "/" + eachFileInfo.FileName)
                #else:
                    # This situation happens when directory is scanned twice => simply ignore this type of duplicate
        lLastCheckSum = eachFileInfo.CheckSum
        lLastFileSize = eachFileInfo.Size
    print( "Wait : " + str( lNbOfCheckSumToCompute ) + " checksums to be computed" )
    myMultipleThreads.waitAllThreadHaveFinished()
    
    #logTimings = Instrumentor.InstrumentationTimer("CheckDuplicates", 0)
    lLastCheckSum       = -1
    lLastFileSize       = -1    
    pProgressIndicator.setTitle("Compare CheckSum")
    pProgressIndicator.setCurrent( 0 )
    for i,eachFileInfo in enumerate(pListOfFiles):
        if ( i != 0 ):
            # eliminate empty files and case where both files are same file (!?!)
            if ( ( eachFileInfo.Size == 0 ) or ( (pListOfFiles[i-1].Path+"/"+pListOfFiles[i-1].FileName) == (eachFileInfo.Path+"/"+eachFileInfo.FileName) ) ):
                if ( eachFileInfo.Size == 0 ):
                    lNbOfEmptyFiles += 1
                    #print("\nFound empty file : " + eachFileInfo.Path + "/" + eachFileInfo.FileName)
                #else:
                    # This situation happens when directory is scanned twice => simply ignore this type of duplicate
            else:
                if ( eachFileInfo.Size == lLastFileSize ):
                    #if( eachFileInfo.CheckSum is None ) or ( lLastCheckSum is None ):
                        #print("Error : CheckSum not computed")
                    # If checkSum are equals, register a new duplicate
                    if ( eachFileInfo.CheckSum == lLastCheckSum ):
                        #print("\nFound duplicate : \n"+pListOfFiles[i-1].Path+"/"+pListOfFiles[i-1].FileName
                        #      +"\n has same checkSum and size ("+str(lLastFileSize)+") as \n"+ eachFileInfo.Path+"/"+eachFileInfo.FileName)
                        lListDuplicates.append(DuplicateFileWithInfo(pListOfFiles[i-1], eachFileInfo))
                        lNumberOfDuplicate  += 1
                        lSumOfDuplicateSize += lLastFileSize
                    #else:
                        #print("*** Found same CheckSum with different size ?!? *** : \n"
                        #      +pListOfFiles[i-1].Path+"/"+pListOfFiles[i-1].FileName+" (size="+fileSizeToStr(pListOfFiles[i-1].Size)
                        #      +")\n has same checkSum as \n"+ eachFileInfo.Path+"/"+eachFileInfo.FileName+" (size="+fileSizeToStr(eachFileInfo.Size)+")")
        lLastCheckSum = eachFileInfo.CheckSum
        lLastFileSize = eachFileInfo.Size
    #logTimings.Stop()
    #Instrumentor.Instrumentor.EndSession()
        
##    for i,uniqueCheckSum in enumerate(CheckSumList):
##        print("CheckSumList["+str(i)+"]="+str(uniqueCheckSum))
    print("*** DONE *** Found " + str(lNumberOfDuplicate)
          + " duplicates (based on CheckSum and size) for a total amount of "+fileSizeToStr(lSumOfDuplicateSize)
          + " (+ " + str(lNbOfEmptyFiles) + " empty files)")
    lListDuplicates.sort(key=attrgetter("File1.Path"))
    pProgressIndicator.setMode( 1 )
    #print(lListDuplicates)
    return(lListDuplicates, lNumberOfDuplicate, lSumOfDuplicateSize)


# exclude from pListOfDuplicates a Directory to exclude from pListOfDuplicates
# example : /path_to_A/fileA is duplicate of /path_to_B/fileB, if exclude /path_to_B => this duplicate will be excluded from pListOfDuplicates
# If pIsExclusionConfirmed, really exclude files, else only print what would be done with confirmation
def excludeDirectory(pListOfDuplicates, pOneDirectoryToExclude, pIsExclusionConfirmed):
    lNbOfFileToExclude = 0
    lMessage           = ""
    lIndex             = len(pListOfDuplicates)-1 # from (len()-1)->0 because from 0->(len()-1) doen't work because of delete !
    while ( lIndex != -1 ): # Cf pListOfDuplicates[len()-1] -> pListOfDuplicates[0] to be managed
        lEachDuplicates = pListOfDuplicates[lIndex]
        if ( ( lEachDuplicates.File1.Path.find(pOneDirectoryToExclude) != -1 ) or
             ( lEachDuplicates.File2.Path.find(pOneDirectoryToExclude) != -1 ) ):
            lNbOfFileToExclude += 1
            if ( pIsExclusionConfirmed ):
                lMessage += "Exclude duplicate :\n"+lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName+ "\nand \n" + lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
                del pListOfDuplicates[lIndex] #remove from list of duplicates
            else:
                lMessage += "Duplicate to exclude :\n"+lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName+ "\nand \n" + lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
        lIndex -= 1
    #print(lMessage)
    if ( pIsExclusionConfirmed ):
        print(str(lNbOfFileToExclude)+" duplicates excluded")
    else:
        print("Found " +str(lNbOfFileToExclude) +" duplicates to exclude")
    return(pListOfDuplicates, lMessage)

# exclude from pListOfDuplicates all duplicates where at least one file is selected
def excludeSelected(pListOfDuplicates):
    lNbOfFileToExclude = 0
    lIndex             = len(pListOfDuplicates)-1 # from (len()-1)->0 because from 0->(len()-1) doen't work because of delete !
    while ( lIndex != -1 ): # Cf pListOfDuplicates[len()-1] -> pListOfDuplicates[0] to be managed
        lEachDuplicates = pListOfDuplicates[lIndex]
        if ( ( lEachDuplicates.File1.IsSelected ) or ( lEachDuplicates.File2.IsSelected ) ):
            lNbOfFileToExclude += 1
            del pListOfDuplicates[lIndex] #remove from list of duplicates
        lIndex -= 1
    lMessage = "Excluded " + str(lNbOfFileToExclude)+" duplicates"
    return(pListOfDuplicates, lMessage)

# delete one file
def deleteOneFile(pFilePath):
    lDeleteSucceeded = False
    if (os.path.isfile(pFilePath)):
        try:
            os.remove(pFilePath) #remove file!
        except:
            print("Failed to delete : "+pFilePath)
        else:
            lDeleteSucceeded = True
    return(lDeleteSucceeded)

# remove one file from a listOfFiles
def removeFileFromListOfFile(pListOfFiles, pPath, pFileName):
    lIndex     = len(pListOfFiles)-1
    lFileFound = False
    while ( ( lIndex > 0 ) and ( not lFileFound ) ):
        if ( ( pListOfFiles[lIndex].Path == pPath ) and ( pListOfFiles[lIndex].FileName == pFileName ) ):
            lFileFound = True
            del pListOfFiles[lIndex]
        lIndex -= 1

# remove duplicates in pListOfDuplicates by selecting the file in pOneDirectoryOfDuplicate
# If pIsDeleteConfirmed, really delete file, else only print what would be done with confirmation
def removeDuplicatesByDirectory(pListOfDuplicates, pOneDirectoryOfDuplicate, pIsDeleteConfirmed):
    lMessage               = ""
    lNbOfFilesDeleted      = 0
    lSumOfDeletedFileSizes = 0
    lIndex = len(pListOfDuplicates)-1 # from (len()-1)->0 because from 0->(len()-1) doesn't work because of suppression !
    #for lEachDuplicates in pListOfDuplicates:
    while ( lIndex != -1 ): # Cf pListOfDuplicates[len()-1] -> pListOfDuplicates[0] to be managed
        lEachDuplicates = pListOfDuplicates[lIndex]
        if ( lEachDuplicates.File1.Path.find(pOneDirectoryOfDuplicate) != -1 ):
            if ( lEachDuplicates.File2.Path.find(pOneDirectoryOfDuplicate) != -1 ):
                # Never delete File 1 AND File 2 !!!
                lMessage += "*********************************\n"
                lMessage += "Found Directory "+pOneDirectoryOfDuplicate+ " in both File1Path and File2Path for :\n"
                lMessage += lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\nand \n"
                lMessage += lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n => Do nothing\n"
                lMessage += "*********************************\n"
            else:
                # pPattern found in File1.Path
                if ( pIsDeleteConfirmed ):
                    if ( deleteOneFile(lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName) ):
                        lMessage += "File deleted :"+lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"
                    del pListOfDuplicates[lIndex] #remove from list of duplicates
                else:
                    lMessage += "File to delete :"+lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"
                lNbOfFilesDeleted +=1
                lSumOfDeletedFileSizes += lEachDuplicates.File1.Size
        else: #not found in File1.Path
            if ( lEachDuplicates.File2.Path.find(pOneDirectoryOfDuplicate) != -1 ):
                # pPattern found in File2.Path
                if ( pIsDeleteConfirmed ):
                    if ( deleteOneFile(lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName) ):
                        lMessage += "File deleted :"+lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
                    del pListOfDuplicates[lIndex] #remove from list of duplicates
                else:
                    lMessage += "File to delete :"+lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
                lNbOfFilesDeleted +=1
                lSumOfDeletedFileSizes += lEachDuplicates.File2.Size
        lIndex -= 1
    if ( pIsDeleteConfirmed ):
        lMessage += "\nDeleted "+str(lNbOfFilesDeleted)+" files for a total amount of "+fileSizeToStr(lSumOfDeletedFileSizes)+"\n"
    else:
        lMessage += "\nFile to delete : "+str(lNbOfFilesDeleted)+" files for a total amount of "+fileSizeToStr(lSumOfDeletedFileSizes)+"\n"
    return(pListOfDuplicates, lMessage)

def unselectIfSelected( pIsSelected, pNbOfFilesSelected, pSumOfSelectedFileSizes, pSize ):
    if( pIsSelected ):
        pIsSelected              = False
        pNbOfFilesSelected      -= 1
        pSumOfSelectedFileSizes -= pSize
    return pIsSelected, pNbOfFilesSelected, pSumOfSelectedFileSizes

def selectIfNotSelected( pIsSelected, pNbOfFilesSelected, pSumOfSelectedFileSizes, pSize ):
    if( not pIsSelected ):
        pIsSelected              = True
        pNbOfFilesSelected      += 1
        pSumOfSelectedFileSizes += pSize
    return pIsSelected, pNbOfFilesSelected, pSumOfSelectedFileSizes


# Select duplicates in pListOfDuplicates:
#                     - if path correspond to pattern pOneDirectoryOfDuplicate if pIsNormal is True
#                     - or does not correspond to pattern if pIsNormal is False (reverse, antippatern)
# if pIsNormal True : If only Path1 correspond to pattern, select File 1. The same for File 2. 
#                     If both correspond to pPattern, select none or both depending on pAllowDoubleSelect
#                     If none of Path 1 and 2 correspond to pattern, select none.
# if pIsNormal False: Do the opposite. pPattern is the criteria for not selecting. Called "reverse".
#                     If only Path1 correspond to pattern, select File 2. The same for Path 2. 
#                     If both correspond to pPattern, select none.
#                     But if none of Path 1 and 2 correspond to pattern, select none.
#                     pPattern must be present on one file to select the other file.
def selectDuplicatesByDirectory(pListOfDuplicates, pOneDirectoryOfDuplicate, pAllowDoubleSelect, pIsNormal):
    lMessage                = ""
    lNbOfFilesSelected      = 0
    lSumOfSelectedFileSizes = 0
    lIndex                  = len(pListOfDuplicates)-1
    #for lEachDuplicates in pListOfDuplicates:
    while ( lIndex != -1 ):
        lEachDuplicates = pListOfDuplicates[lIndex]
        if ( lEachDuplicates.File1.Path.find(pOneDirectoryOfDuplicate) != -1 ):
            # pPattern found in Path1
            if ( lEachDuplicates.File2.Path.find(pOneDirectoryOfDuplicate) != -1 ):
                # pPattern also found in Path2
                if ( pIsNormal ):
                    if ( pAllowDoubleSelect ):
                        lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                        lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                        #lMessage += "-> Select1 " + str(lIndex) + " : " + lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"
                        #lMessage += "-> Select2 " + str(lIndex) + " : " + lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
                    else:
                        # Find in both Path1 and Path2 but do not allowDoubleSelect => Unselect both File 1 and File 2 !!!
                        lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                        lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                        #lMessage += "-> " + str(lIndex) + " Found Pattern "+pOneDirectoryOfDuplicate+ " in both File1Path and File2Path for :\n"
                        #lMessage += lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\nand \n"
                        #lMessage += lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n => Do nothing\n\n"
                else:
                    # else (reverse) found antiPattern in both Path1 and Path2 => Unselect both File 1 and File 2 !!!
                    lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                    lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                    #lMessage += "-> " + str(lIndex) + " Found antiPattern "+pOneDirectoryOfDuplicate+ " in both File1Path and File2Path for :\n"
                    #lMessage += lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\nand \n"
                    #lMessage += lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n => Do nothing\n\n"
            else:
                # Pattern found in File1.Path but not in File2.Path
                if ( pIsNormal ):
                    # select file 1
                    lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                    if( not pAllowDoubleSelect ):
                        lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                    #lMessage += "-> Select1 " + str(lIndex) + " : " + lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"
                else: # reverse
                    # select file 2
                    if( not pAllowDoubleSelect ):
                        lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected ( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                    lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                    #lMessage += "-> Select2 " + str(lIndex) + " : " + lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
        else:  # pPattern not found in Path1
            if ( lEachDuplicates.File2.Path.find(pOneDirectoryOfDuplicate) != -1 ):
                # Pattern found in File2.Path
                if ( pIsNormal ):
                    # Select File2
                    if( not pAllowDoubleSelect ):
                        lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected ( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                    lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                    #lMessage += "-> Select2 " + str(lIndex) + " : " + lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
                else: # reverse
                    # Select File 1
                    lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                    if( not pAllowDoubleSelect ):
                        lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                    #lMessage += "-> Select1 " + str(lIndex) + " : " + lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"                    
        lIndex -= 1
    #lMessage += "\nSelected "+str(lNbOfFilesSelected)+" files for a total amount of "+fileSizeToStr(lSumOfSelectedFileSizes)+"\n"
    return(pListOfDuplicates, lMessage)


# remove duplicates from pListOfDuplicates corresponding to a pattern pPattern
# pPattern can be a directory, a part of directory ("/svg/"), or a filename pattern (.svg, .old)
# If pIsDeleteConfirmed, really delete file, else only print what would be done with confirmation
def deleteByPatternInFileName(pListOfDuplicates, pPattern, pIsDeleteConfirmed):
    lMessage               = ""
    lNbOfFilesDeleted      = 0
    lSumOfDeletedFileSizes = 0
    lIndex = len(pListOfDuplicates)-1 # from (len()-1)->0 because from 0->(len()-1) doen't work because of delete !
    #for lEachDuplicates in pListOfDuplicates:
    while ( lIndex != -1 ): # Cf pListOfDuplicates[len()-1] -> pListOfDuplicates[0] to be managed
        lEachDuplicates = pListOfDuplicates[lIndex]
        if ( lEachDuplicates.File1.FileName.find(pPattern) != -1 ):
            if ( lEachDuplicates.File2.FileName.find(pPattern) != -1 ):
                # Never delete File 1 AND File 2 !!!
                lMessage += "*********************************\n"
                lMessage += "Found Pattern "+pPattern+ " in both FileName1 and FileName2 for :\n"
                lMessage += lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\nand \n"
                lMessage += lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n => Do nothing\n"
                lMessage += "*********************************\n"
            else:
                # pPattern found in File1.FileName
                if ( pIsDeleteConfirmed ):
                    if ( deleteOneFile(lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName) ):
                        lMessage += "File deleted :"+lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"
                    del pListOfDuplicates[lIndex] #remove from list of duplicates
                else:
                    lMessage += "File to delete :"+lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"
                lNbOfFilesDeleted +=1
                lSumOfDeletedFileSizes += lEachDuplicates.File1.Size
        else: 
            if ( lEachDuplicates.File2.FileName.find(pPattern) != -1 ):
                # pPattern found in File2.FileName
                if ( pIsDeleteConfirmed ):
                    if ( deleteOneFile(lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName) ):
                        lMessage += "File deleted :"+lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
                    del pListOfDuplicates[lIndex] #remove from list of duplicates
                else:
                    lMessage += "File to delete :"+lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
                lNbOfFilesDeleted +=1
                lSumOfDeletedFileSizes += lEachDuplicates.File2.Size
        lIndex -= 1
    if ( pIsDeleteConfirmed ):
        lMessage += "\nDeleted "+str(lNbOfFilesDeleted)+" files for a total amount of "+fileSizeToStr(lSumOfDeletedFileSizes)+"\n"
    else:
        lMessage += "\nFile to delete : "+str(lNbOfFilesDeleted)+" files for a total amount of "+fileSizeToStr(lSumOfDeletedFileSizes)+"\n"
    return(pListOfDuplicates, lMessage)


# Select duplicates in pListOfDuplicates if path correspond to pattern pOneDirectoryOfDuplicate
def selectDuplicatesByFileName(pListOfDuplicates, pOneDirectoryOfDuplicate, pAllowDoubleSelect):
    lMessage                = ""
    lNbOfFilesSelected      = 0
    lSumOfSelectedFileSizes = 0
    lIndex                  = len(pListOfDuplicates)-1
    #for lEachDuplicates in pListOfDuplicates:
    while ( lIndex != -1 ):
        lEachDuplicates = pListOfDuplicates[lIndex]
        if ( lEachDuplicates.File1.FileName.find(pOneDirectoryOfDuplicate) != -1 ):
            # pPattern found in FileName1
            if ( lEachDuplicates.File2.FileName.find(pOneDirectoryOfDuplicate) != -1 ):
                # pPattern found in FileName2
                if ( pAllowDoubleSelect ):
                    lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                    lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                    #lMessage += "-> Select1 " + str(lIndex) + " : " + lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"
                    #lMessage += "-> Select2 " + str(lIndex) + " : " + lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
                else:
                    # do not select both File 1 and File 2 !!!
                    lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                    lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                    #lMessage += "-> " + str(lIndex) + " Found Pattern "+pOneDirectoryOfDuplicate+ " in both File1Name and File2Name for :\n"
                    #lMessage += lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\nand \n"
                    #lMessage += lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n => Do nothing\n\n"
            else:
                # pPattern found in FileName1 but not in FileName2
                lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                if( not pAllowDoubleSelect ):
                    lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected ( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                #lMessage += "-> Select1 " + str(lIndex) + " : " + lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName + "\n"
        else: 
            # pPattern not found in FileName1
            if ( lEachDuplicates.File2.FileName.find(pOneDirectoryOfDuplicate) != -1 ):
                # pPattern found in FileName2
                if( not pAllowDoubleSelect ):
                    lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = unselectIfSelected ( lEachDuplicates.File1.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size )
                lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes = selectIfNotSelected( lEachDuplicates.File2.IsSelected, lNbOfFilesSelected, lSumOfSelectedFileSizes, lEachDuplicates.File1.Size ) #File1 and File2 has same size
                #lMessage += "-> Select2 " + str(lIndex) + " : " + lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName + "\n"
        lIndex -= 1
    #lMessage += "\nSelected "+str(lNbOfFilesSelected)+" files for a total amount of "+fileSizeToStr(lSumOfSelectedFileSizes)+"\n"
    return(pListOfDuplicates, lMessage)


#display all duplicates found
def displayDuplicates(pListOfDuplicates):
    lSumOfFileSizes = 0
    print("File1\tFile2\tFileSize")
    for lEachDuplicates in pListOfDuplicates:
        lSumOfFileSizes += lEachDuplicates.File1.Size
        print(lEachDuplicates.File1.Path+"/"+lEachDuplicates.File1.FileName+"\t\n"
              +lEachDuplicates.File2.Path+"/"+lEachDuplicates.File2.FileName+"\t"
              +str(lEachDuplicates.File1.Size))
    print("Found "+str(len(pListOfDuplicates))+" files for a total of "+fileSizeToStr(lSumOfFileSizes)+ ".\n\n")


# *** main ***
# only for non GUI interface (launch on command line)
##if __name__ == "__main__":
##    MyListOfFiles = list()
##    #Read config file with :
##    #- Directories to scan
##    #- Directories to exclude
##    # if not found, do this ->
##    if False:
##        Directory = expanduser("~")
##        scanDirectory(Directory, MyListOfFiles, 0)
##        #print(MyListOfFiles)
##        print("Scan finished. Found "+str(len(MyListOfFiles))+" Files")
##    else:
##        pOneDirectory = raw_input("Which Directory would you like to scan ? : ")
##        while (pOneDirectory != ""):
##            # save in config file input directories
##            scanDirectory(pOneDirectory, MyListOfFiles, 0)
##            pOneDirectory = raw_input("Add another Directory ? : ")
##
##    if ( len(MyListOfFiles) == 0 ):
##        print("No file found -> exit. \nBye !")
##        sys.exit(0)
##
##    # find duplicates
##    MyListOfDuplicates, nbOfDuplicates, totalDuplicateFileSize = findDuplicates(MyListOfFiles)
##    displayDuplicates(MyListOfDuplicates)
##
##    # propose to exclude duplicates because in special directory
##    pOneDirectoryToExclude = raw_input("Which Directory would you like to exclude (type return to skip) ? : ")
##    while (pOneDirectoryToExclude != ""):
##        lMessage = ""
##        MyListOfDuplicates, lMessage = excludeDirectory(MyListOfDuplicates, pOneDirectoryToExclude, False)
##        print(lMessage)
##        confirm = raw_input("Confirm exclusion ? (Y/N) : ")
##        if (confirm == "Y" ):
##            lMessage = ""
##            MyListOfDuplicates, lMessage = excludeDirectory(MyListOfDuplicates, pOneDirectoryToExclude, True)
##            print(lMessage)
##            # save in config file excluded directories
##        displayDuplicates(MyListOfDuplicates)
##        pOneDirectoryToExclude = raw_input("Which Directory would you like to exclude (type return to exit) ? : ")
##
##    # propose to delete files by directory
##    lOneDirectoryOfDuplicates = raw_input("Which Directory can be considered as containing duplicates (type return to skip) ? : ")
##    while (lOneDirectoryOfDuplicates != ""):
##        MyListOfDuplicates, lMessage = removeDuplicatesByDirectory(MyListOfDuplicates, lOneDirectoryOfDuplicates, False)
##        print(lMessage)
##        confirm = raw_input("Confirm delete ? (Y/N) : ")
##        if (confirm == "Y" ):
##            MyListOfDuplicates, lMessage = removeDuplicatesByDirectory(MyListOfDuplicates, lOneDirectoryOfDuplicates, True)
##            print(lMessage)
##            # save in config file dupkicates directories ?
##        displayDuplicates(MyListOfDuplicates)
##        lOneDirectoryOfDuplicates = raw_input("Which Directory can be considered as containing duplicates (type return to skip) ? : ")
##
##    # ask if user want to delete file containing string (old, svg, unisson, etc.)
##    OnePatternToDelete = raw_input("Pattern (old, svg, unisson, etc.) to search in filename for delete (type return to skip) ? :")
##    while ( OnePatternToDelete != "" ):
##        MyListOfDuplicates, lMessage = deleteByPatternInFileName(MyListOfDuplicates, OnePatternToDelete, False)
##        print(lMessage)
##        confirm = raw_input("Confirm delete ? (Y/N) : ")
##        if (confirm == "Y" ):
##            MyListOfDuplicates, lMessage = deleteByPatternInFileName(MyListOfDuplicates, OnePatternToDelete, True)
##            print(lMessage)
##            # save in config file patterns ?
##        displayDuplicates(MyListOfDuplicates)
##        OnePatternToDelete = raw_input("Pattern (old, svg, unisson, etc.) to search in filename for delete (type return to skip) ? :")
##
##    # ask if user wants to delete a specific file, then ask again until user hit return
##
##    # list all empty directories and propose to delete them
