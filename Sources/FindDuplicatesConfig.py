#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on Octobre the 21th, 2017

@author: florent Pichot
'''

import os.path
import xml.etree.ElementTree

import FindDuplicates

class FindDuplicateConfig:
    def __init__(self):
        self.ConfigFilePath                  = ""
        self.AllowDoubleSelect               = "0"
        self.ListOfDirectoryToScan           = list()
        self.ListOfPatternToExclude          = list()
        self.ListOfPatternInFilePathToSelect = list()
        self.ListOfPatternInFileNameToSelect = list()

    def reset(self):
        self.AllowDoubleSelect = "0"
        FindDuplicates.clearList(self.ListOfDirectoryToScan)
        FindDuplicates.clearList(self.ListOfPatternToExclude)
        FindDuplicates.clearList(self.ListOfPatternInFilePathToSelect)
        FindDuplicates.clearList(self.ListOfPatternInFileNameToSelect)

    def getConfigFile(self):
        return(self.ConfigFilePath)
    
    def setAllowDoubleSelect(self, AllowDoubleSelect):
        if ( AllowDoubleSelect ):
            self.AllowDoubleSelect = "1"
        else:
            self.AllowDoubleSelect = "0"

    def getAllowDoubleSelect(self):
        returnValue = False
        if ( self.AllowDoubleSelect == "1" ):
            returnValue = True
        return(returnValue)

    def addDirectoryToScan(self, DirectoryToScan):
        self.ListOfDirectoryToScan.append(DirectoryToScan)

    def getNbOfDirectoryToScan(self):
        return(len(self.ListOfDirectoryToScan))

    def getLastDirectoryToScan(self):
        returnValue = ""
        ListLength = len(self.ListOfDirectoryToScan)
        if ( ListLength != 0 ):
            returnValue = self.ListOfDirectoryToScan[ListLength-1]
        return(returnValue)
        
    def addPatternToExclude(self, PatternToExclude):
        self.ListOfPatternToExclude.append(PatternToExclude)
    
    def addPatternInFilePathToSelect(self, PatternInFilePathToSelect):
        self.ListOfPatternInFilePathToSelect.append(PatternInFilePathToSelect)

    def addPatternInFileNameToSelect(self, PatternInFileNameToSelect):
        self.ListOfPatternInFileNameToSelect.append(PatternInFileNameToSelect)

    def getLastPattern(self):
        returnValue = ""
        ListLength = len(self.ListOfPatternInFileNameToSelect)
        if ( ListLength != 0 ):
            returnValue = self.ListOfPatternInFileNameToSelect[ListLength-1]
        else:
            ListLength = len(self.ListOfPatternInFilePathToSelect)
            if ( ListLength != 0 ):
                returnValue = self.ListOfPatternInFilePathToSelect[ListLength-1]
            else:
                ListLength = len(self.ListOfPatternToExclude)
                if ( ListLength != 0 ):
                    returnValue = self.ListOfPatternToExclude[ListLength-1]
        return(returnValue)

    def loadFromConfigFile(self, configFile):
        if ( os.path.isfile(configFile) ):
            self.ConfigFilePath = configFile
            treeOnConfig = xml.etree.ElementTree.parse(configFile)
            rootOnConfig = treeOnConfig.getroot()
            for eachAllowDoubleSelect in rootOnConfig.findall("Selection"): # There shall be only one !
                self.AllowDoubleSelect = "0"
                if ( eachAllowDoubleSelect.get("AllowDoubleSelect") == "1" ):
                    self.AllowDoubleSelect = "1"                    
            for eachDirectoryToScan in rootOnConfig.findall("DirectoryToScan"):
                self.ListOfDirectoryToScan.append(eachDirectoryToScan.get("Path"))
            for eachPatternToExclude in rootOnConfig.findall("PatternToExclude"):
                self.ListOfPatternToExclude.append(eachPatternToExclude.get("Pattern"))
            for eachPatternInFilePathToSelect in rootOnConfig.findall("PatternInFilePathToSelect"):
                self.ListOfPatternInFilePathToSelect.append(eachPatternInFilePathToSelect.get("Pattern"))
            for eachPatternInFileNameToSelect in rootOnConfig.findall("PatternInFileNameToSelect"):
                self.ListOfPatternInFileNameToSelect.append(eachPatternInFileNameToSelect.get("Pattern"))
            print("Load config Ok")
        else:
            print("File not found")

    def saveToConfigFile(self, configFilePath):
        returnValue = False
        Config = xml.etree.ElementTree.Element("FindDuplicatedConfig")
        AllowDoubleSelectCfg = xml.etree.ElementTree.SubElement(Config, "Selection")
        AllowDoubleSelectCfg.set("AllowDoubleSelect", self.AllowDoubleSelect)
        for eachDirectoryToScan in self.ListOfDirectoryToScan:
            eachDirectoryToScanCfg = xml.etree.ElementTree.SubElement(Config, "DirectoryToScan")
            eachDirectoryToScanCfg.set("Path", eachDirectoryToScan)
        for eachPatternToExclude in self.ListOfPatternToExclude:
            eachPatternToExcludeCfg = xml.etree.ElementTree.SubElement(Config, "PatternToExclude")
            eachPatternToExcludeCfg.set("Pattern", eachPatternToExclude)
        for eachPatternInFilePathToSelect in self.ListOfPatternInFilePathToSelect:
            eachPatternInFilePathToSelectCfg = xml.etree.ElementTree.SubElement(Config, "PatternInFilePathToSelect")
            eachPatternInFilePathToSelectCfg.set("Pattern", eachPatternInFilePathToSelect)
        for eachPatternInFileNameToSelect in self.ListOfPatternInFileNameToSelect:
            eachPatternInFileNameToSelectCfg = xml.etree.ElementTree.SubElement(Config, "PatternInFileNameToSelect")
            eachPatternInFileNameToSelectCfg.set("Pattern", eachPatternInFileNameToSelect)
        with open(configFilePath,"wt") as configFile:
            try:
                xmlString = xml.etree.ElementTree.tostring(Config, encoding='unicode', method='xml')
                print(xmlString)
                configFile.write(xmlString) 
                #myElementTree = xml.etree.ElementTree(Config)
                #myElementTree.write(configFile)
            #except:
            #    print("Unexpected error:", sys.exc_info()[0])
            #    returnValue = False
            except Exception as inst:
                print(type(inst))   # the exception instance
                print(inst.args)    # arguments stored in .args
                print(inst)         # __str__ allows args to be printed directly,
                                    # but may be overridden in exception subclasses      
            else:
                returnValue = True
        return(returnValue)
        
    def applyConfig(self, pProgressIndicator):
        ListOfFiles                = list()
        ListOfDuplicates           = list()
        nbOfFilesFound             = 0
        sumOfTheseScannedFileSizes = 0
        for eachDirectoryToScan in self.ListOfDirectoryToScan:
            nbOfFilesFound += FindDuplicates.countNbOfFiles(eachDirectoryToScan, pProgressIndicator)
        print("Found a total of " + str(nbOfFilesFound) + " files to scan")
        pProgressIndicator.setMax(nbOfFilesFound)
        for eachDirectoryToScan in self.ListOfDirectoryToScan:
            print("Scan : " + eachDirectoryToScan)
            oneNbOfFilesFound, OneSumOfTheseScannedFileSizes = FindDuplicates.scanDirectory(eachDirectoryToScan, ListOfFiles, sumOfTheseScannedFileSizes, pProgressIndicator)
            nbOfFilesFound             += oneNbOfFilesFound
            sumOfTheseScannedFileSizes += OneSumOfTheseScannedFileSizes
        ListOfDuplicates, nbOfDuplicates, totalSizeOfDuplicates = FindDuplicates.findDuplicates(ListOfFiles, sumOfTheseScannedFileSizes, pProgressIndicator)
        for eachPattern in self.ListOfPatternToExclude:
            print("Exclude :" + eachPattern)
            ListOfDuplicates, message = FindDuplicates.excludeDirectory(ListOfDuplicates, eachPattern, True)
        for eachPattern in self.ListOfPatternInFilePathToSelect:
            print("Select files with pattern " + eachPattern + " in Directory")
            ListOfDuplicates, message = FindDuplicates.selectDuplicatesByDirectory(ListOfDuplicates, eachPattern, (self.AllowDoubleSelect == "1"), True)
        for eachPattern in self.ListOfPatternInFileNameToSelect:
            print("Select files with pattern " + eachPattern + " in FileName")
            ListOfDuplicates, message = FindDuplicates.selectDuplicatesByFileName(ListOfDuplicates, eachPattern, (self.AllowDoubleSelect == "1"))
        # no need to return nbOfDuplicates and totalSizeOfDuplicates because it will be computed during display in ListCtrl
        return(ListOfFiles, ListOfDuplicates, nbOfFilesFound, sumOfTheseScannedFileSizes)


