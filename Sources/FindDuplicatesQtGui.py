#!/usr/bin/python
# -*- coding: utf-8 -*-
# for python v3
#from PyQt5.Qt import QMenu

# '''
# Created on March the 22th, 2020

# @author: florent Pichot

# '''

import os, sys, getopt
#import time
#from os.path import expanduser
from operator import attrgetter
from PyQt5 import QtCore, QtWidgets, QtGui
import Qt5GeneratedGui  # import du fichier QtGeneratedGui.py généré par pyuic5
# cd /home/florent/workspace/FindDuplicates/FindDuplicates
# python -m PyQt5.uic.pyuic -x -o Qt5GeneratedGui.py FindDuplicates_Qt_GUI/fdup_guimain.ui
from Qt5GeneratedScrollDialogBox import Ui_MyQDialog

import FindDuplicates
import FindDuplicatesConfig

# in order to easily change colon order of self.ui.TV_list_ctrl
COL_PATH1  = 0
COL_FILE1  = 1
COL_CHECK1 = 2
COL_CHECK2 = 3
COL_PATH2  = 4
COL_FILE2  = 5
COL_SIZE   = 6

class ScrollDialogBox(QtWidgets.QDialog):
    def __init__(self, pParent, pTitle, pMessageList, *args, **kwargs):
        QtWidgets.QDialog.__init__(self, *args, **kwargs)
        self.parent = pParent
        self.initUI(pTitle, pMessageList, *args, **kwargs)
    
    def initUI(self, pTitle, pMessageList, *args, **kwargs):
        if ( len(pMessageList) == 0):
            pTitle = "Nothing to delete"
            self.setWindowTitle(pTitle)
        else:
            self.setWindowTitle("Please confirm")
        MyConfirmDialog = Ui_MyQDialog()
        MyConfirmDialog.setupUi(self)
        MyConfirmDialog.ButB_buttonBox.accepted.connect(self.onConfirm);
        MyConfirmDialog.ButB_buttonBox.rejected.connect(self.onCancel);
        MyConfirmDialog.Labl_ExplanationMsg.setText(pTitle)
        self.VBlayout = QtWidgets.QVBoxLayout() #self.widget)
        for item in pMessageList:
            self.VBlayout.addWidget(QtWidgets.QLabel(item))
        MyConfirmDialog.scrollAreaWidgetContents.setLayout(self.VBlayout)
        self.setModal(True)
        #self.show()
    
    def onConfirm(self):
        print("Delete confirmed !!!")
        self.parent.deleteSelectedWithConfirmation(True)
    
    def onCancel(self):
        print("Delete canceled")        

class MyDuplicateTableModel(QtCore.QAbstractTableModel): 
    def __init__(self, parent=None, *args): 
        super(MyDuplicateTableModel, self).__init__()
        self.parent = parent
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.NbOfElements = 0
        self.datatable = None
        #self.headerdata = None
        #self.headers = [ 'Path1', 'File1', 'X1', 'X2', 'Path2', 'File2', 'Size']
        #self.dataFrame = None
        self.nbOfSelected = 0
        self.totalSizeOfSelected = 0
        self.totalSizeOfDuplicates = 0
        self.model = QtGui.QStandardItemModel(self)
        self.model.setHorizontalHeaderLabels(['Path1', 'File1', '1', '2', 'Path2', 'File2', 'FileSize'])
        parent.ui.TV_list_ctrl.setModel(self.model)
        parent.ui.TV_list_ctrl.clicked.connect(self.onListLeftClick)
        #parent.ui.TV_list_ctrl.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        parent.ui.TV_list_ctrl.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        #parent.ui.TV_list_ctrl.customContextMenuRequested.connect(parent.ui.TV_list_ctrl, QtCore.pyqtSignal(self.customContextMenuRequested(QtCore.QPoint)), self, QtCore.pyqtSlot(self.displayMenu(QtCore.QPoint)))
        #parent.ui.TV_list_ctrl.customContextMenuRequested.connect(self.onListRightClick)

    def updateCheckBox(self, pRow, pColumn, pIsCheck, pWasCheck, pSize):
        if ( pIsCheck ):
            #self.model.setItem(pRow, pColumn, QtGui.QStandardItem("X"))
            self.model.item(pRow, pColumn).setText("X")
            if( not pWasCheck ):
                self.nbOfSelected        += 1                    
                self.totalSizeOfSelected += pSize
        else:
            # not checked
            #self.model.setItem(pRow, pColumn, QtGui.QStandardItem(" "))
            self.model.item(pRow, pColumn).setText(" ")
            if( pWasCheck ):
                self.nbOfSelected        -= 1                    
                self.totalSizeOfSelected -= pSize
        self.model.item(pRow, pColumn).setTextAlignment(QtCore.Qt.AlignCenter|QtCore.Qt.AlignVCenter)

    def createCheckBox(self, pRow, pColumn, pIsCheck, pWasCheck, pSize):
        if ( pIsCheck ):
            self.model.setItem(pRow, pColumn, QtGui.QStandardItem("X"))
            if( not pWasCheck ):
                self.nbOfSelected        += 1                    
                self.totalSizeOfSelected += pSize
        else:
            # not checked
            self.model.setItem(pRow, pColumn, QtGui.QStandardItem(" "))
            if( pWasCheck ):
                self.nbOfSelected        -= 1                    
                self.totalSizeOfSelected -= pSize
        self.model.item(pRow, pColumn).setTextAlignment(QtCore.Qt.AlignCenter|QtCore.Qt.AlignVCenter)
                        
    def update(self, pListOfDuplicates, pFirstCharOfPathToKeep):
        ##headers = dataIn.columns.values
        ##header_items = [
        ##    str(field)
        ##    for field in headers
        ##]
        ##self.headerdata = header_items
        lSavedColumnWidth = []
        lOneColIdx = 0
        while( lOneColIdx <= COL_SIZE):
            try:
                lSavedColumnWidth.append( self.parent.ui.TV_list_ctrl.columnWidth(lOneColIdx) )
            except:
                lSavedColumnWidth.append( 0 )
            lOneColIdx = lOneColIdx + 1
        #print("Saved columnWidth: ")
        #print(lSavedColumnWidth)
        self.model.clear()
        self.datatable = pListOfDuplicates
        self.nbOfSelected = 0
        self.totalSizeOfSelected = 0
        self.totalSizeOfDuplicates = 0
        index = 0
        fichierErrNom = open("ErrFileEncoding.csv", "w") # to log files with encoding errors
        for eachDuplicate in pListOfDuplicates:
            step = 1
            try :
                self.model.setItem(index, QtGui.QStandardItem(eachDuplicate.File1.FileName))
                self.createCheckBox(index, COL_CHECK1, eachDuplicate.File1.IsSelected, False, eachDuplicate.File1.Size)
                self.createCheckBox(index, COL_CHECK2, eachDuplicate.File2.IsSelected, False, eachDuplicate.File2.Size)
                step += 1
                self.model.setItem(index, COL_PATH1, QtGui.QStandardItem(eachDuplicate.File1.Path[pFirstCharOfPathToKeep:]))
                step += 1
                self.model.setItem(index, COL_FILE1, QtGui.QStandardItem(eachDuplicate.File1.FileName))
                step += 1
                self.model.setItem(index, COL_PATH2, QtGui.QStandardItem(eachDuplicate.File2.Path[pFirstCharOfPathToKeep:]))
                step += 1
                self.model.setItem(index, COL_FILE2, QtGui.QStandardItem(eachDuplicate.File2.FileName))
                self.model.setItem(index, COL_SIZE,  QtGui.QStandardItem(FindDuplicates.fileSizeToStr(eachDuplicate.File1.Size)))
            except :
                # @TODO self.model.removeRow(self.index);
                print("UTF8 error with following duplicates, on step "+str(step)+":\n"+eachDuplicate.File1.Path+"/"+eachDuplicate.File1.FileName+"\n"
                      +eachDuplicate.File2.Path+"/"+eachDuplicate.File2.FileName)
                #fichierErrNom.write("UTF8 error with following duplicates, on step "+str(step)+":\n"+eachDuplicate.File1.Path+"/"+eachDuplicate.File1.FileName+"\n"
                #      +eachDuplicate.File2.Path+"/"+eachDuplicate.File2.FileName+"\n\n")
                try:
                    if ( step == 1 ):
                        #print("Err FileName1 :\n"+fix_text(eachDuplicate.File1.FileName))
                        fichierErrNom.write("\nErr FileName1\t"+eachDuplicate.File1.Path+"\t"+eachDuplicate.File1.FileName)
                    if ( step == 2 ):
                        #print("Err Path1 :\n"+fix_text(eachDuplicate.File1.Path))
                        fichierErrNom.write("\nErr FilePath1\t"+eachDuplicate.File1.Path)
                    if ( step == 4 ):
                        #print("Err Path2 :\n"+fix_text(eachDuplicate.File2.Path))
                        fichierErrNom.write("\nErr FilePath2\t"+eachDuplicate.File2.Path)
                    if ( step == 5 ):
                        #print("Err FileName2 :\n"+eachDuplicate.File2.Path+"/"+fix_text(eachDuplicate.File2.FileName))
                        fichierErrNom.write("\nErr FileName2\t"+eachDuplicate.File2.Path+"\t"+eachDuplicate.File2.FileName)
                except:
                    print("Failed again at step "+str(step))
            else:
                #no exception
                self.totalSizeOfDuplicates += eachDuplicate.File1.Size
                index += 1
        fichierErrNom.close()
        self.NbOfElements = index
        lOneColIdx = 0
        while( lOneColIdx <= COL_SIZE):
            lSavedColWidth = lSavedColumnWidth[lOneColIdx]
            if( ( lSavedColWidth != 0) and ( lSavedColWidth != 100 ) ): # 100 is default width
                self.parent.ui.TV_list_ctrl.setColumnWidth(lOneColIdx, lSavedColumnWidth[lOneColIdx])
            else:
                self.parent.ui.TV_list_ctrl.resizeColumnToContents(lOneColIdx)
            lOneColIdx = lOneColIdx + 1        

    def rowCount(self, parent=QtCore.QModelIndex()):
        return self.NbOfElements

    def columnCount(self, parent=QtCore.QModelIndex()):
        return 7

    def getNbOfSelected(self):
        return self.nbOfSelected
    
    def getTotalSizeOfSelected(self):
        return self.totalSizeOfSelected
    
    def getTotalSizeOfDuplicates(self):
        return self.totalSizeOfDuplicates

    def data(self, index, role=QtCore.Qt.DisplayRole): 
        if not index.isValid(): 
            return QtCore.QVariant()
        elif role != QtCore.Qt.DisplayRole: 
            return QtCore.QVariant() 
        #return QtCore.QVariant(self.model.data(index))
        else:
            #return QtCore.QVariant(self.model.data(index))
            # ***** @TODO *****
            return QtCore.QVariant() 

    def headerData(self, col, orientation, role):
        return QtCore.QVariant()
        #if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
        #    return QtCore.QVariant()
        #return QtCore.QVariant(self.headerdata[col])

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        # @TODO
        #print "setData", index.row(), index.column(), value
        print("SetData ["+str(index.row())+","+str(index.column)+"] = "+str(value))

    def flags(self, index):
        if ( (index.column() == COL_CHECK1) or (index.column() == COL_CHECK2) ):
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        else:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def onListLeftClick(self, item):
        lRow    = item.row()
        lColumn = item.column()
        lUpdateStats = False
        # This test seems not to be necessary
        if ( ( lRow >= 0 ) and ( lRow < len(self.datatable) ) ):
            if ( lColumn == COL_CHECK1 ):
                #invert selection
                lWasSelected = self.datatable[lRow].File1.IsSelected
                self.datatable[lRow].File1.IsSelected = not lWasSelected
                #update table
                self.updateCheckBox(lRow, lColumn, not lWasSelected, lWasSelected, self.datatable[lRow].File1.Size)
                if( not self.parent.AllowDoubleSelect):
                    lOtherWasSelected = self.datatable[lRow].File2.IsSelected
                    if( ( not lWasSelected ) and (lOtherWasSelected) ):
                        # deactivate other
                        self.datatable[lRow].File2.IsSelected = False
                        self.updateCheckBox(lRow, COL_CHECK2, False, True, self.datatable[lRow].File1.Size)
                lUpdateStats = True
            if ( lColumn == COL_CHECK2 ):
                #invert selection
                lWasSelected = self.datatable[lRow].File2.IsSelected
                self.datatable[lRow].File2.IsSelected = not lWasSelected
                #update table
                self.updateCheckBox(lRow, lColumn, not lWasSelected, lWasSelected, self.datatable[lRow].File1.Size)
                if( not self.parent.AllowDoubleSelect):
                    lOtherWasSelected = self.datatable[lRow].File1.IsSelected
                    if( ( not lWasSelected ) and (lOtherWasSelected) ):
                        # deactivate other
                        self.datatable[lRow].File1.IsSelected = False
                        self.updateCheckBox(lRow, COL_CHECK1, False, True, self.datatable[lRow].File1.Size)
                lUpdateStats = True
            if( lUpdateStats ):
                self.parent.showStatistics()
            else:
                #print("Row=" + str(lRow) + " column=" + str(lColumn))
                pasteToClipBoard = ""
                if( lColumn == 0 ):
                    # copy path 1 in clipboard
                    pasteToClipBoard = self.datatable[lRow].File1.Path
                if( lColumn == 1 ):
                    # copy path 1 in clipboard
                    pasteToClipBoard = self.datatable[lRow].File1.Path + "/" + self.datatable[lRow].File1.FileName
                if( lColumn == 4 ):
                    # copy path 1 in clipboard
                    pasteToClipBoard = self.datatable[lRow].File2.Path
                if( lColumn == 5 ):
                    # copy path 1 in clipboard
                    pasteToClipBoard = self.datatable[lRow].File2.Path + "/" + self.datatable[lRow].File2.FileName
                if( not ( pasteToClipBoard == "" ) ):
                    #print(pasteToClipBoard)
                    cb = QtWidgets.QApplication.clipboard()
                    cb.clear(mode=cb.Clipboard )
                    cb.setText(pasteToClipBoard, mode=cb.Clipboard)

    #===========================================================================
    # def CopyPath1(self, event):
    #     print("CopyPath1 called")
    #     # get the selected row and column
    #     #row = self.tableWidget.rowAt(event.pos().y())
    #     row = self.tableWidget.rowAt(event.pos().y())
    #     col = self.tableWidget.columnAt(event.pos().x())
    #     # get the selected cell
    #     cell = self.tableWidget.item(row, col)
    #     # get the text inside selected cell (if any)
    #     cellText = cell.text()
    #     # get the widget inside selected cell (if any)
    #     widget = self.tableWidget.cellWidget(row, col)
    #===========================================================================
    
    # https://stackoverflow.com/questions/20930764/how-to-add-a-right-click-menu-to-each-cell-of-qtableview-in-pyqt
    # TV_DuplicateList is a QTableView which has contextMenuEvent()
    # ==> Il faudrait implémenter une fonction contextMenuEvent dans une class GalleryUi(QtGui.QTableView)
    # Or nous, nous utilisons la class standard de base : self.TV_DuplicateList = QtGui.QTableView(self.verticalLayoutWidget)
    # Nous c'est le modèle qu'on a développé : QAbstractTableModel
    #  :-(
    
    # open a window and diplay Path1, Path1\File1, Path2and Path2\File2 to allow simple copy paste to explorer or file viewer or dif program
    # https://stackoverflow.com/questions/47849484/how-to-get-the-row-number-from-a-rigth-click-on-a-qtableview
    #===========================================================================
    # def onRightClick(self, QPos=None):
    #     self.rcMenu=QtGui.QMenu(self)
    #     self.rcMenu.addAction('Copy Path1')       
    #     self.rcMenu.addAction('Copy Path2')       
    #     self.rcMenu.addAction('Copy File1')       
    #     self.rcMenu.addAction('Copy File2')       
    #     self.rcMenu.move(QPos)
    #     self.rcMenu.show()
    #===========================================================================
                    
    #===========================================================================
    # def onListRightClick(self, item):
    #     print(item.pos())
    #     index = self.indexAt(item.pos()) CRASH CRASH CRASH
    #     if ( index != -1 ):
    #         title    = "For easy copy and paste in other programs"
    #         message  = "Path1 : \n"
    #         message += self.ListOfDuplicates[index].File1.Path     + "\n"
    #         message += "Path2 : \n"
    #         message += self.ListOfDuplicates[index].File2.Path     + "\n"
    #         message += "File1 : \n"
    #         message += self.ListOfDuplicates[index].File1.Path + "/" + self.ListOfDuplicates[index].File1.FileName + "\n"
    #         message += "File2 : \n"
    #         message += self.ListOfDuplicates[index].File2.Path + "/" + self.ListOfDuplicates[index].File2.FileName + "\n"
    #         print("Message : " + message)
    #         msgBox = QtWidgets.QMessageBox
    #         msgBox.question(self, title, message, msgBox.Ok)
    #     else:
    #         print("index = -1")  
    #===========================================================================
                    
    #===========================================================================
    # def contextMenuEvent(self, event):
    #     index = self.indexAt(event.pos())
    #     if ( index != -1 ):
    #         title    = "For easy copy and paste in other programs"
    #         message  = "Path1 : \n"
    #         message += self.ListOfDuplicates[index].File1.Path     + "\n"
    #         message += "Path2 : \n"
    #         message += self.ListOfDuplicates[index].File2.Path     + "\n"
    #         message += "File1 : \n"
    #         message += self.ListOfDuplicates[index].File1.Path + "/" + self.ListOfDuplicates[index].File1.FileName + "\n"
    #         message += "File2 : \n"
    #         message += self.ListOfDuplicates[index].File2.Path + "/" + self.ListOfDuplicates[index].File2.FileName + "\n"
    #         print("Message : " + message)
    #         msgBox = QtWidgets.QMessageBox
    #         msgBox.question(self, title, message, msgBox.Ok)
    #     else:
    #         print("index = -1")
    #===========================================================================

    #===========================================================================
    # def displayMenu(self, pos):
    #     print(pos)
    #===========================================================================

    # if     deletionConfirmed : delete files corresponding to duplicate file **selected** in self.list_ctrl
    # if not deletionConfirmed : show files that would be deleted if deletionConfirmed was True
    def deleteSelectedWithConfirmation(self, deletionConfirmed):
        message1 = "Confirm suppression of following files on disk : \n"
        lList2 = []
        index = 0
        #for eachDuplicates in self.parent.ListOfDuplicates:
        while( index < self.NbOfElements ):
            #AtLeastOneFileDeleted = False
            Delete1 = self.datatable[index].File1.IsSelected #self.list_ctrl.GetItemText(index, COL_CHECK1)
            Delete2 = self.datatable[index].File2.IsSelected #self.list_ctrl.GetItemText(index, COL_CHECK2)
            FullePath1 = self.datatable[index].File1.Path + "/" + self.datatable[index].File1.FileName
            FullePath2 = self.datatable[index].File2.Path + "/" + self.datatable[index].File2.FileName
            if ( Delete1 ):
                if ( deletionConfirmed ):
                    if FindDuplicates.deleteOneFile( FullePath1 ):
                        print("Delete file : " + FullePath1 )
                        FindDuplicates.removeFileFromListOfFile(self.parent.ListOfFiles, self.datatable[index].File1.Path, self.datatable[index].File1.FileName)
                    else:
                        # suppression failed => Check if file has not already been deleted (on another duplicate line)
                        if os.path.exists( FullePath1 ):
                            print("Failed to delete file : " + FullePath1 )
                else:
                    lList2.append("Delete : " + FullePath1 )
            if ( Delete2 ):
                if ( deletionConfirmed ):
                    if FindDuplicates.deleteOneFile( FullePath2 ):
                        print("Delete file : " + FullePath2 )
                        FindDuplicates.removeFileFromListOfFile(self.parent.ListOfFiles, self.datatable[index].File2.Path, self.datatable[index].File2.FileName)
                    else:
                        # suppression failed => Check if file has not already been deleted (on another duplicate line)
                        if os.path.exists( FullePath2 ):
                            print("Failed to delete file : " + FullePath2 )
                else:
                    lList2.append("Delete : " + FullePath2 )
            index += 1
        if ( deletionConfirmed ):
            # Compute new ListOfDuplicates after file suppression
            self.parent.PeformFindDuplicateThread.start()
        else:
            # Display confirmation dialog
            self.MyScrollDialog = ScrollDialogBox(self, message1, lList2)
            self.MyScrollDialog.show()            


class ProgressIndicator(QtWidgets.QDialog):
    eventUpdateHMI = QtCore.pyqtSignal(int)
        
    def __init__(self):
        super().__init__()
        #self.eventUpdateHMI = QtCore.pyqtSignal(int)
        self.maxValue     = 0
        self.currentValue = 0
        self.mode= 1 # mode 1 : nb of item,  mode 2 : size
        self.title = ""
        self.setWindowTitle('Progress Indicator')
        self.progress = QtWidgets.QProgressBar(self)
        self.progress.setGeometry(0, 0, 300, 25)
        self.progressAsText = QtWidgets.QLineEdit(self)
        self.progressAsText.setGeometry(0, 0, 300, 25)
        self.progressAsText.setText("Ready to start")
        self.progressAsText.move(0,25)
        self.setModal(True)
        self.show()
    
    # only call this function in Gui thread !!!
    def updateDisplay(self, pValue):
        if( pValue == 0 ):
            # Hide at the end
            self.progressAsText.setText("")
            self.hide()
        elif( pValue == 1 ):
            # show again
            if( self.mode == 1 ):
                self.progress.setValue(self.currentValue)
            else:
                if( self.maxValue == 0 ):
                    self.progress.setValue( 1 ) # 1%
                else:
                    self.progress.setValue( int( self.currentValue / self.maxValue * 100 ) )
            self.progressAsText.setText( self.title + " : " + "In progress...")
            self.show()
        else:
            # update max (2) or current (3)
            if( pValue == 2 ):
                if( self.mode == 1):
                    self.progress.setMaximum(self.maxValue)
                else:
                    self.progress.setMaximum(100)
            else:
                if( self.mode == 1):
                    self.progress.setValue(self.currentValue)
                else:
                    if( self.maxValue == 0 ):
                        self.progress.setValue( 1 ) # 1%
                    else:
                        self.progress.setValue( int( self.currentValue / self.maxValue * 100 ) )
            # update test
            if( self.maxValue == 0 ):
                self.progressAsText.setText( self.title + " : " + "0/" + str(self.currentValue))
            else:
                if( self.mode == 1 ):
                    self.progressAsText.setText( self.title + " : " + str(self.currentValue) + "/" + str(self.maxValue))
                else:
                    self.progressAsText.setText( self.title + " : " + FindDuplicates.fileSizeToStr(self.currentValue) + "/" + FindDuplicates.fileSizeToStr(self.maxValue))
            
    def hideAtTheEnd(self):
        #print("hideAtTheEnd")
        self.maxValue     = 0
        self.currentValue = 0
        self.eventUpdateHMI.emit(0)
            
    def showAgain(self):
        #print("ShowAgain")
        self.eventUpdateHMI.emit(1)
    
    def setMax(self, pMaxValue):
        #print("max " + str(pMaxValue))
        self.maxValue = pMaxValue
        self.eventUpdateHMI.emit(2)
        
    def setCurrent(self, pCurrentValue):
        #print("Current " + str(pCurrentValue))
        self.currentValue = pCurrentValue
        self.eventUpdateHMI.emit(3)
    
    def incrementCurrent( self, pDelta ):
        self.currentValue += pDelta
        self.eventUpdateHMI.emit(3)
    
    def setMode( self, pMode ):
        self.currentValue = 0
        #self.maxValue     = 1
        self.mode         = pMode
    
    def setTitle( self, pTitle ):
        self.title = pTitle
        self.eventUpdateHMI.emit(3)


class ScanInAThread(QtCore.QThread):
    #eventComputeIsOver = QtCore.pyqtSignal(int)
    
    def setParams(self, pParent, pProgressIndicator):
        self.parent = pParent
        self.progressIndicator = pProgressIndicator
            
    def run(self):
        print("ScanInAThread run")
        self.progressIndicator.setTitle( "Count number of files" )
        self.progressIndicator.setMode(1)
        self.progressIndicator.showAgain()
        #try:
        nbOfFilesFound = FindDuplicates.countNbOfFiles(self.parent.directoryToAddString, self.progressIndicator )
        self.progressIndicator.setMax(nbOfFilesFound)
        nbOfFilesFound, sumOfTheseScannedFileSizes = FindDuplicates.scanDirectory(self.parent.directoryToAddString, self.parent.ListOfFiles, self.parent.sumOfAllScannedFileSizes, self.progressIndicator)
        self.parent.lastScannedDirectory      = self.parent.directoryToAddString
        self.parent.nbOfAllScannedFiles      += nbOfFilesFound
        self.parent.sumOfAllScannedFileSizes += sumOfTheseScannedFileSizes
        self.parent.ListOfDuplicates, self.parent.nbOfDuplicates, self.parent.totalSizeOfDuplicates = FindDuplicates.findDuplicates(self.parent.ListOfFiles, self.parent.sumOfAllScannedFileSizes, self.progressIndicator)
        #except:
        #    print("ScanInAThread failed")
        #else:
        #    print("ScanInAThread done")
        #self.eventComputeIsOver.emit(0)
        self.progressIndicator.hideAtTheEnd()

class FindDuplicatesInAThread(QtCore.QThread):
    def setParams(self, pParent, pProgressIndicator):
        self.parent = pParent
        self.progressIndicator = pProgressIndicator
        
    def run(self):
        print("FindDuplicatesInAThread run")
        self.progressIndicator.showAgain()
        self.parent.ListOfDuplicates, self.parent.nbOfDuplicates, self.parent.totalSizeOfDuplicates = FindDuplicates.findDuplicates(self.parent.ListOfFiles, self.parent.sumOfAllScannedFileSizes, self.progressIndicator)
        print("FindDuplicatesInAThread done")
        #self.eventComputeIsOver.emit(0)
        self.progressIndicator.hideAtTheEnd()

class LoadConfigInAThread(QtCore.QThread):
    #eventComputeIsOver = QtCore.pyqtSignal(int)

    def setParams(self, pParent, pProgressIndicator):
        self.parent = pParent
        self.progressIndicator = pProgressIndicator
        
    def run(self):
        print("LoadConfigInAThread run")
        self.progressIndicator.showAgain()
        self.parent.ListOfFiles, self.parent.ListOfDuplicates, self.parent.nbOfAllScannedFiles, self.parent.sumOfAllScannedFileSizes = self.parent.config.applyConfig(self.progressIndicator)
        print("LoadConfigInAThread done")
        #self.eventComputeIsOver.emit(0)
        self.progressIndicator.hideAtTheEnd()

#class MyWindow(QtWidgets.QMainWindow):
class MyWindow(QtWidgets.QMainWindow):

    def __init__(self, pConfig, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.ui = Qt5GeneratedGui.Ui_FDup_GuiMain()
        self.ui.setupUi(self)
        self.config                   = FindDuplicatesConfig.FindDuplicateConfig()
        self.DuplicateTableModel      = MyDuplicateTableModel(self)
        #self.ui.TV_list_ctrl.setModel(self.DuplicateTableModel)
        self.ListOfFiles              = list()
        self.ListOfDuplicates         = list()
        self.homeDirectory            = os.path.expanduser("~")
        self.directoryToAddString     = os.path.expanduser("~")
        if ( self.config.getNbOfDirectoryToScan() != 0 ):
            self.directoryToAddString = self.config.getLastDirectoryToScan()
        self.clearResults()
        self.config                   = pConfig
        self.lastScannedDirectory     = ""
        self.lastSort                 = ""
        self.lastSortWasReversed      = False
        self.patternString            = ""
        self.AllowDoubleSelect        = False
        self.ui.actionAllow_double_select.setChecked(self.AllowDoubleSelect)
        # use all screen and make listCtrl as high as possible
        # @TODO
        # Add a panel so it looks the correct way on all platforms
        # @TODO
        
        self.Winflags = self.windowFlags()
        self.Winflags |= QtCore.Qt.CustomizeWindowHint; # In order to activate Maximize button
        #self.Winflags |= QtCore.Qt.WindowMaximizeButtonHint
        self.setWindowFlags(self.Winflags)
        
        self.ui.LE_StatusBar.setText("Ready. Please specify directory to scan.")
        
        self.ui.actionExit.triggered.connect                ( self.actionQuit                      )
        self.ui.actionLoad_configuration.triggered.connect  ( self.loadAndApplyConfig              )
        self.ui.actionSave_Configuration.triggered.connect  ( self.saveConfig                      )
        
        self.ui.actionHelp.triggered.connect                ( self.actionHelp                      )
        self.ui.actionAbout.triggered.connect               ( self.actionAbout                     )
        self.ui.actionAllow_double_select.triggered.connect ( self.actionSwitchDoubleSelectAllowed )

        self.ui.PB_SelectDir.clicked.connect     ( self.onSelectDirectory  )
        self.ui.PB_Add.clicked.connect           ( self.onAddDirectory     )
        self.ui.PB_Clear.clicked.connect         ( self.onClearDirectories )
        
        self.ui.PB_SortPath1.clicked.connect     ( lambda: self.onSort(COL_PATH1  ) )
        self.ui.PB_SortPath2.clicked.connect     ( lambda: self.onSort(COL_PATH2  ) )
        self.ui.PB_SortFileName1.clicked.connect ( lambda: self.onSort(COL_FILE1  ) )
        self.ui.PB_SortFileName2.clicked.connect ( lambda: self.onSort(COL_FILE2  ) )
        self.ui.PB_SortSelection.clicked.connect ( lambda: self.onSort(COL_CHECK1 ) ) 
        self.ui.PB_SortSize.clicked.connect      ( lambda: self.onSort(COL_SIZE   ) )

        self.ui.PB_FilterByPatternInPath.clicked.connect    ( self.onFilterByPatternInPath     )
        self.ui.PB_FilterByAntiPatternInPath.clicked.connect( self.onFilterByAntiPatternInPath )
        self.ui.PB_FilterByPatternInName.clicked.connect    ( self.onFilterByPatternInName     )

        self.ui.PB_Exclude.clicked.connect  ( self.onExcludeSelected )
        self.ui.PB_Delete.clicked.connect   ( self.onDeleteSelected  )
        self.ui.PB_Unselect.clicked.connect ( self.onUnselectAll     )
        
        self.ProgressIndicator = ProgressIndicator()
        self.ProgressIndicator.eventUpdateHMI.connect(self.onUpdateHMI)
        self.ProgressIndicator.hide()
        
        self.PerformScanInAThread = ScanInAThread()
        self.PerformScanInAThread.setParams(self, self.ProgressIndicator)
        
        self.PeformFindDuplicateThread = FindDuplicatesInAThread()
        self.PeformFindDuplicateThread.setParams(self, self.ProgressIndicator)
        
        self.LoadConfigInAThread = LoadConfigInAThread()
        self.LoadConfigInAThread.setParams(self, self.ProgressIndicator)
        
        if( self.config.getNbOfDirectoryToScan() != 0 ):
            self.applyConfig("NoEvent")

    def contextMenuEvent(self, event):
        #print("In contextMenuEvent !")
        #self.myMenu = QMenu(self)
        #getPath1Action = QAction('Get Path1', self)
        #self.myMenu.addAction(getPath1Action)
        currentPos = event.globalPos()
        print("In contextMenuEvent : ", currentPos)
        #getPath1Action.triggered.connect( lambda: self.getPath1(currentPos))
        #self.myMenu.popup(currentPos)

    def getPath1(self, q_point):
        row = self.tableView.rowAt( self.tableView.viewport().mapFromGlobal(q_point).y())
        print("row =" + str(row))

    def onUpdateHMI(self, pValue):
        #print("onUpdateHMI " + str(pValue))
        if( pValue == 0 ):
            #print("Hide progress bar and update list")
            self.ui.LE_StatusBar.setText("Hide progress bar and update list")
            self.ProgressIndicator.setTitle("Update display")
            try:
                self.fillListCtrlWithListOfDuplicates()
                self.ui.LE_StatusBar.setText("Scanning done")
            except:
                print("Failed to execute fillListCtrlWithListOfDuplicates")
                self.ui.LE_StatusBar.setText("Scanning failed")
            #self.ProgressIndicator.hideAtTheEnd()
            #self.ProgressIndicator.hide()
            self.ProgressIndicator.updateDisplay(0)
        if( pValue == 1 ):
            #print("Show again")
            self.ProgressIndicator.updateDisplay(1)
        if( pValue == 2 ):
            # update Max
            #print("Update Max")
            self.ProgressIndicator.updateDisplay(2)
        if( pValue == 3 ):
            # update Current
            #print("Update Current")
            self.ProgressIndicator.updateDisplay(3)

    def onSelectDirectory(self):
        self.config.reset()
        self.clearResults()
        self.onAddDirectory()
        #selectedDir = QtWidgets.QFileDialog.getExistingDirectory(self,"Select directory", expanduser("~"), QtWidgets.QFileDialog.ShowDirsOnly)
    
    def onAddDirectory(self):
        self.directoryToAddString = QtWidgets.QFileDialog.getExistingDirectory(self,"Select another directory", self.config.getLastDirectoryToScan(), QtWidgets.QFileDialog.ShowDirsOnly)
        print("Directory selected : " + self.directoryToAddString)
        # remove trailing slash
        if self.directoryToAddString.endswith('/'):
            self.directoryToAddString = self.directoryToAddString[:-1]
        self.ui.LE_StatusBar.setText("Scan in progress")
        if ( self.lastScannedDirectory == self.directoryToAddString ):
            print("Directory " + self.directoryToAddString + " already scanned")
            self.ui.LE_StatusBar.setText("Directory " + self.directoryToAddString + " already scanned")
            # TODO : display popup message ?
        else:
            self.config.addDirectoryToScan(self.directoryToAddString)
            self.PerformScanInAThread.start()

    def onClearDirectories(self):
        self.config.reset()
        self.clearResults()
        self.displayList()

    def computeIsSortReversed(self, pNewSort):
        returnValue = False
        if( self.lastSort == pNewSort ):
            returnValue = not self.lastSortWasReversed
        self.lastSort            = pNewSort
        self.lastSortWasReversed = returnValue

    # for "SortByCheck1" : "SortByCheck1"-N -> "SortByCheck1"-R -> "SortByCheck2"-N -> "SortByCheck2"-R -> "SortByCheck1"-N etc.
    # this function is only called with pNewSort = "SortByCheck1"
    def computeIsSortReversedForCheck(self, pNewSort):
        lNewReversedSort = False
        lFilter = attrgetter("File1.IsSelected", "File2.IsSelected")
        if( self.lastSort == "SortByCheck2" ):
            if not self.lastSortWasReversed:
                # 2N -> 2R
                pNewSort         = "SortByCheck2"
                lNewReversedSort = True
                lFilter          = attrgetter("File2.IsSelected", "File1.IsSelected")   
            #else:
                # 2R -> 1N
                #lNewReversedSort = False
        else:
            if( self.lastSort == pNewSort ):
                if( self.lastSortWasReversed ):
                    # 1R -> 2N
                    pNewSort        = "SortByCheck2"
                    #lNewReversedSort = False
                    lFilter          = attrgetter("File2.IsSelected", "File1.IsSelected")                    
                else:
                    # 1N -> 1R
                    lNewReversedSort = True
            #else:
                # other -> 1N
                #lNewReversedSort = False
        self.lastSort            = pNewSort
        self.lastSortWasReversed = lNewReversedSort
        return lFilter

    def onSort(self, pColumn):
        self.ProgressIndicator.updateDisplay(1)
        if( pColumn == COL_PATH1 ):
            self.computeIsSortReversed("SortByPath1")
            self.ListOfDuplicates.sort(key=attrgetter("File1.Path","File1.FileName"), reverse=self.lastSortWasReversed)
        if( pColumn == COL_PATH2 ):
            self.computeIsSortReversed("SortByPath2")
            self.ListOfDuplicates.sort(key=attrgetter("File2.Path","File2.FileName"), reverse=self.lastSortWasReversed)
        if( pColumn == COL_FILE1 ):
            self.computeIsSortReversed("SortByFile1")
            self.ListOfDuplicates.sort(key=attrgetter("File1.FileName"), reverse=self.lastSortWasReversed)
        if( pColumn == COL_FILE2 ):
            self.computeIsSortReversed("SortByFile2")
            self.ListOfDuplicates.sort(key=attrgetter("File2.FileName"), reverse=self.lastSortWasReversed)
        if( pColumn == COL_SIZE ):
            self.computeIsSortReversed("SortBySize")
            self.ListOfDuplicates.sort(key=attrgetter("File1.Size"), reverse=not self.lastSortWasReversed)
        if( pColumn == COL_CHECK1 ):
            lFilter = self.computeIsSortReversedForCheck("SortByCheck1")
            self.ListOfDuplicates.sort( key=lFilter, reverse=not self.lastSortWasReversed )
        if( pColumn == COL_CHECK2 ):
            self.computeIsSortReversed("SortByCheck2")
            self.ListOfDuplicates.sort(key=attrgetter("File2.IsSelected", "File1.IsSelected"), reverse=not self.lastSortWasReversed)
        self.ProgressIndicator.updateDisplay(0)
        self.displayList()

    def onFilterByPatternInPath(self):
        self.ProgressIndicator.updateDisplay(1)
        self.patternString = self.ui.LE_Pattern.text()
        self.config.addPatternInFilePathToSelect(self.patternString)
        self.ListOfDuplicates, message = FindDuplicates.selectDuplicatesByDirectory(self.ListOfDuplicates, self.patternString, self.AllowDoubleSelect, True)
        self.ProgressIndicator.updateDisplay(0)
        #print("Message:" + message)
        self.displayList()

    def onFilterByAntiPatternInPath(self):
        self.ProgressIndicator.updateDisplay(1)
        self.patternString = self.ui.LE_Pattern.text()
        self.config.addPatternInFilePathToSelect(self.patternString)
        self.ListOfDuplicates, message = FindDuplicates.selectDuplicatesByDirectory(self.ListOfDuplicates, self.patternString, self.AllowDoubleSelect, False)
        self.ProgressIndicator.updateDisplay(0)
        #print("Message:" + message)
        self.displayList()
    
    def onFilterByPatternInName(self):
        self.ProgressIndicator.updateDisplay(1)
        self.patternString = self.ui.LE_Pattern.text()
        self.config.addPatternInFilePathToSelect(self.patternString)
        self.ListOfDuplicates, message = FindDuplicates.selectDuplicatesByFileName(self.ListOfDuplicates, self.patternString, self.AllowDoubleSelect)
        self.ProgressIndicator.updateDisplay(0)
        #print("Message:" + message)
        self.displayList()
    
    def onExcludeSelected(self):
        self.ProgressIndicator.updateDisplay(1)
        self.ListOfDuplicates, message = FindDuplicates.excludeSelected(self.ListOfDuplicates)
        self.ProgressIndicator.updateDisplay(0)
        #print("Message:" + message)
        self.displayList()

    def onDeleteSelected(self):
        #self.ProgressIndicator.updateDisplay(1)
        self.DuplicateTableModel.deleteSelectedWithConfirmation(False)
        #self.ProgressIndicator.updateDisplay(0)

    def onUnselectAll(self):
        self.ProgressIndicator.updateDisplay(1)
        for eachDuplicate in self.ListOfDuplicates:
            eachDuplicate.File1.IsSelected = False
            eachDuplicate.File2.IsSelected = False
        self.ProgressIndicator.updateDisplay(0)
        self.displayList()

    def showStatistics(self):
        self.nbOfDuplicates        = self.DuplicateTableModel.rowCount(0)
        self.nbOfSelected          = self.DuplicateTableModel.getNbOfSelected()
        self.totalSizeOfSelected   = self.DuplicateTableModel.getTotalSizeOfSelected()
        self.totalSizeOfDuplicates = self.DuplicateTableModel.getTotalSizeOfDuplicates()
        if ( len(self.ListOfDuplicates) != self.nbOfDuplicates ):
            print("Found "+str(len(self.ListOfDuplicates))+" files but only "+str(self.nbOfDuplicates)+" can be displayed, for an amount of "
                  +FindDuplicates.fileSizeToStr(self.totalSizeOfDuplicates)+ "")
        #else:
        #    print("Found "+str(self.nbOfDuplicates)+" files for an amount of " + FindDuplicates.fileSizeToStr(self.totalSizeOfDuplicates)+ "")
        self.ui.LE_Nb.setText("Nb of duplicates : "+str(self.nbOfDuplicates)+"/"+str(self.nbOfAllScannedFiles))
        self.ui.LE_Total.setText("Total : "+FindDuplicates.fileSizeToStr(self.totalSizeOfDuplicates)+"/"+FindDuplicates.fileSizeToStr(self.sumOfAllScannedFileSizes))
        self.ui.LE_Selected.setText("Nb of files selected : "+str(self.nbOfSelected)+"/"+str(2*self.nbOfDuplicates))
        self.ui.LE_TotalSelected.setText("Total selected : "+FindDuplicates.fileSizeToStr(self.totalSizeOfSelected)+"/"+FindDuplicates.fileSizeToStr(2*self.totalSizeOfDuplicates))

    def getFirstCharOfPathToKeep(self):
        FirstCharOfPathToKeep = 0
        if ( self.config.getNbOfDirectoryToScan() == 1 ):
            FirstCharOfPathToKeep = len(self.config.getLastDirectoryToScan())
        return FirstCharOfPathToKeep
    
    def displayList(self):
        FirstCharOfPathToKeep = self.getFirstCharOfPathToKeep()
        self.DuplicateTableModel.update(self.ListOfDuplicates, FirstCharOfPathToKeep)
        self.showStatistics()
        
    # fill self.ui.TV_list_ctrl with content of self.ListOfDuplicates
    def fillListCtrlWithListOfDuplicates(self):
        self.ui.TV_list_ctrl.clearSpans()
        self.nbOfDuplicates        = 0
        self.totalSizeOfDuplicates = 0
        self.nbOfSelected          = 0
        self.totalSizeOfSelected   = 0
        self.computeIsSortReversed("SortByPath1")
        self.ListOfDuplicates.sort(key=attrgetter("File1.Path","File1.FileName"), reverse=self.lastSortWasReversed)
        self.displayList()

    # load config file and apply this config
    def loadAndApplyConfig(self):
        configFilePath, _ = QtWidgets.QFileDialog.getOpenFileName(self,"Select configuration file to load", "","All Files (*)")
        if configFilePath:
            self.clearResults()
            self.ui.LE_StatusBar.setText("Load Config file " + configFilePath + " -> Please wait scan in progress...")
            message = "Scan in progress : "+configFilePath
            self.config.reset()
            self.config.loadFromConfigFile(configFilePath)
            self.commonApplyConfig(message)

    def saveConfig(self):
        configFilePath, _ = QtWidgets.QFileDialog.getSaveFileName(self,"Select configuration file to save", "","All Files (*)")
        if ( configFilePath != "" ):
            self.config.setAllowDoubleSelect( self.AllowDoubleSelect )
            if ( self.config.saveToConfigFile( configFilePath ) ):
                self.ui.LE_StatusBar.setText("Configuration saved")
            else:
                self.ui.LE_StatusBar.setText("Failed to save configuration in "+configFilePath)

    # load config file and apply this config
    def applyConfig(self, event):
        self.clearResults()
        self.ui.LE_StatusBar.setText("Apply Config -> Please wait scan in progress...")
        message = "Scan in progress"
        self.commonApplyConfig(message)

    def commonApplyConfig(self, message):
        #hourGlass = wx.lib.agw.pybusyinfo.PyBusyInfo(message, parent=self.panel, title="Please wait")
        #hourGlass.Show(True)
        #hourGlass.Update()
        self.AllowDoubleSelect = self.config.getAllowDoubleSelect()
        self.ui.actionAllow_double_select.setChecked(self.AllowDoubleSelect)
        self.LoadConfigInAThread.start()
        #self.ListOfFiles, self.ListOfDuplicates, self.nbOfAllScannedFiles, self.sumOfAllScannedFileSizes = self.config.applyConfig()
        #self.fillListCtrlWithListOfDuplicates()
        #hourGlass.Show(False)
        #self.ui.directoryToAdd.SetPath(self.config.getLastDirectoryToScan())
        #TODO ? self.pattern.Clear()
        self.ui.LE_Pattern.setText(self.config.getLastPattern())
        self.ui.LE_StatusBar.setText("Configuration loading")


    # Clear previous results : scanned files and duplicates found
    def clearResults(self):
        #self.config.reset() # do not reset config !!!
        FindDuplicates.clearList(self.ListOfFiles)
        FindDuplicates.clearList(self.ListOfDuplicates)
        self.lastScannedDirectory     = ""
        self.lastSort                 = ""
        self.nbOfAllScannedFiles      = 0
        self.sumOfAllScannedFileSizes = 0
        self.nbOfDuplicates           = 0
        self.totalSizeOfDuplicates    = 0
    
    def actionSwitchDoubleSelectAllowed(self):
        self.AllowDoubleSelect = self.ui.actionAllow_double_select.isChecked()
        
    def actionHelp(self):
        print("actionHelp: Not yet implemented")
        
    def actionAbout(self):
        message=QtWidgets.QMessageBox()
        message.setWindowTitle("About FindDuplicates")
        message.setText("FindDuplicates v2.0")
        message.setInformativeText("Find duplicate files in various directories based on file size and hash, not on file name.")
        message.setStandardButtons(QtWidgets.QMessageBox.Ok)
        message.exec_()

    def actionQuit(self):
        QtCore.QCoreApplication.quit()
    
#if __name__ == '__main__':
#    import sys
#    app = QtWidgets.QApplication(sys.argv)
#    window = MyWindow()
#    window.show()
#    sys.exit(app.exec_())
 
def main(argv):
    argsPresent  = True
    MyConfig     = FindDuplicatesConfig.FindDuplicateConfig()
    try:
        opts, args = getopt.getopt(argv,"hc:d:s:e:",["help","config=","directory","select","exclude:"])
    except getopt.GetoptError:
        argsPresent = False
    if ( argsPresent ):
        #print("Args found : ", opts)
        for opt, arg in opts:
            if (opt in ("-h", "--help") ):
                print('Usage: FindDuplicates -c ConfigFile.xml -d DirectoryToScan -s SelectionPattern -e ExcludePattern')
                print('You can provide multiple path to scan (-p).')
                print('You can provide multiple pattern to exclude (-e). Files with path corresponding to this pattern will not be considered as duplicates.')
                print('You can provide multiple pattern to select (-s). Files with path corresponding to this pattern will be selected in interface.')
                sys.exit()
            elif opt in ("-c", "--config"):
                MyConfig.loadFromConfigFile(arg)
            elif opt in ("-d", "--directory"):
                MyConfig.addDirectoryToScan(arg)
            elif opt in ("-s", "--select"):
                MyConfig.addPatternInFilePathToSelect(arg)
            elif opt in ("-e", "--exclude"):
                MyConfig.addPatternToExclude(arg)
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow(MyConfig)
    FDIcon = QtGui.QIcon( "FindDuplicates.ico" );
    window.setWindowIcon( FDIcon );
    window.show()
    #window.showMaximized()
    sys.exit(app.exec_())

if __name__ == "__main__":  
    main(sys.argv[1:]) 
