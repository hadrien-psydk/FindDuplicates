# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FindDuplicates_Qt_GUI/scrolldialogbox.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MyQDialog(object):
    def setupUi(self, MyQDialog):
        MyQDialog.setObjectName("MyQDialog")
        MyQDialog.setWindowModality(QtCore.Qt.WindowModal)
        MyQDialog.resize(841, 300)
        MyQDialog.setSizeGripEnabled(True)
        MyQDialog.setModal(True)
        self.ButB_buttonBox = QtWidgets.QDialogButtonBox(MyQDialog)
        self.ButB_buttonBox.setGeometry(QtCore.QRect(320, 260, 231, 32))
        self.ButB_buttonBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.ButB_buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.ButB_buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.ButB_buttonBox.setObjectName("ButB_buttonBox")
        self.SCRA_scrollArea = QtWidgets.QScrollArea(MyQDialog)
        self.SCRA_scrollArea.setGeometry(QtCore.QRect(9, 53, 821, 201))
        self.SCRA_scrollArea.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.SCRA_scrollArea.setWidgetResizable(True)
        self.SCRA_scrollArea.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.SCRA_scrollArea.setObjectName("SCRA_scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 819, 199))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.SCRA_scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.Labl_ExplanationMsg = QtWidgets.QLabel(MyQDialog)
        self.Labl_ExplanationMsg.setGeometry(QtCore.QRect(8, 7, 821, 41))
        self.Labl_ExplanationMsg.setObjectName("Labl_ExplanationMsg")

        self.retranslateUi(MyQDialog)
        self.ButB_buttonBox.accepted.connect(MyQDialog.accept)
        self.ButB_buttonBox.rejected.connect(MyQDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(MyQDialog)

    def retranslateUi(self, MyQDialog):
        _translate = QtCore.QCoreApplication.translate
        MyQDialog.setWindowTitle(_translate("MyQDialog", "Dialog"))
        self.Labl_ExplanationMsg.setText(_translate("MyQDialog", "TextLabel"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MyQDialog = QtWidgets.QDialog()
    ui = Ui_MyQDialog()
    ui.setupUi(MyQDialog)
    MyQDialog.show()
    sys.exit(app.exec_())

