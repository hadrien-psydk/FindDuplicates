# Basic instrumentation profiler by Cherno turned to Python by Florent
# import Instrumentor

# In functions, create object InstrumentationTimer to log events
#def myFct():
#    localData = InstrumentationTimer("myFct")
#    # do something...
#    # on localData destruction, it will write timing info of myFct into graph info

# Instrumentor.EndSession()

# if your programm does not call EndSession (crash), juste add ]} at the end of result.json

# open Chrome web browser at adress : chrome://tracing, click on Load and select results.json

import time
from threading import RLock

class C_Instrumentor:
    def __init__(self, pSessionName, pFileName = "results.json"):
        self.CurrentSession   = pSessionName
        self.FileName         = pFileName
        self.OutputStream     = None
        self._IsActive        = False # will be set to True by BeginSession
        self.ProfileCount     = 0
        self.Mutex            = RLock()
        self.BeginSession()

    def __del__( self ):
        self.EndSession()
    
    def IsActive( self ):
        return( self._IsActive )

    def BeginSession( self ):
        if( ( not self._IsActive ) or ( self.OutputStream is None ) ):
            try:
                self.OutputStream = open( self.FileName, "wt" )
                self._IsActive    = True
                self.WriteHeader()
            except OSError:
                print( "Failed to create file " + self.FileName )

    def EndSession( self ):
        if( self._IsActive ):
            self.WriteFooter()
            self._IsActive = False
            self.OutputStream.close()
            self.OutputStream     = None
            self.ProfileCount = 0

    def WriteProfile( self, pName, pStartTimepoint, pEndTimepoint, pThreadID ):
        with self.Mutex:
            if( self.ProfileCount > 0 ):
                self.OutputStream.write( "," )
            self.ProfileCount += 1
            pName = pName.replace('\\', '/' )
            #self.OutputStream.write('\n{"cat":"function","dur":' + str( pEndTimepoint - pStartTimepoint ) + ',"name":"' + pName + '","ph":"X","pid":0,"tid":' + str( pThreadID ) + ',"ts":' + str( pStartTimepoint ) + '}' )
            self.OutputStream.write('\n{"cat":"function","dur":' + str( pEndTimepoint - pStartTimepoint ) + ',"name":"' + pName + '","ph":"X","pid":0,"tid":1,"ts":' + str( pStartTimepoint ) + '}' )
            #self.OutputStream.flush();

    def Flush( self ):
        if( self._IsActive ):
            self.OutputStream.flush()

    def WriteHeader( self ):
        self.OutputStream.write( '{"otherData": {},"traceEvents":[' )
        self.OutputStream.flush()

    def WriteFooter( self ):
        self.OutputStream.write( "]}" )
        self.OutputStream.flush()

Instrumentor = C_Instrumentor( "Graph" )


class InstrumentationTimer:
    def __init__( self, pName, pId = 0, pIsStopped = False ):
        self.Name     = pName
        self.Id       = pId
        self.Stopped  = pIsStopped
        self._IsActive = Instrumentor.IsActive()
        if( ( self._IsActive ) and ( not self.Stopped ) ):
            self.StartTimepoint = 1000000 * time.time()   # s ==> us

    def __del__( self ):
        if( not self.Stopped ):
            self.Stop()

    # only useful for Timer created in Stopped mode
    def Start( self ):
        self._IsActive = Instrumentor.IsActive() # compute it again because possibiliy not active when object created.
        if( self._IsActive ):
            self.StartTimepoint = 1000000 * time.time() # s ==> us
            self.Stopped = False

    def Stop( self ):
        if( self._IsActive ):
            lEndTimepoint = 1000000 * time.time()  # s ==> us
            Instrumentor.WriteProfile( self.Name, self.StartTimepoint, lEndTimepoint, self.Id );
            self.Stopped = True;
