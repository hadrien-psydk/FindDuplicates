@echo off

@set currentPath=%cd%
@set destDir=TempDup
@set python3Path="C:\Program Files\Python39\python.exe"

@echo Create duplicates for tests
@mkdir %destDir%                          >nul 2>&1
@copy /Y File1.txt %destDir%\File1Bis.txt >nul 2>&1
@copy /Y File1.txt %destDir%\File1Ter.txt >nul 2>&1
@copy /Y File1.txt %destDir%\File1Qua.txt >nul 2>&1
@copy /Y File2.txt %destDir%\File2Bis.txt >nul 2>&1
@copy /Y File3.txt %destDir%\File3Bis.txt >nul 2>&1

@echo Launch FindDuplicate
@rem echo %python3Path% %currentPath%/../FindDuplicatesQtGui.py -d %currentPath%\ -s %destDir%
@rem %python3Path% %currentPath%/../FindDuplicatesQtGui.py -d %currentPath%\ -s %destDir%
@echo %python3Path% %currentPath%/../FindDuplicatesQtGui.py -c TestConfig.xml
@%python3Path% %currentPath%/../FindDuplicatesQtGui.py -c TestConfig.xml

