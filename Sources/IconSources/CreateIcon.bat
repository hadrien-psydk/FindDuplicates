@echo off

@rem *********************************************************************************************************************
@rem *** For Windows only ***
@rem Generate ico file from 3 different resolutions of the icon

@rem Note: magick can be found here : 
@rem   set ImageMagicPath below where batch file can find it : full path or exe name if in system path of in FindDuplicates directory

@rem *********************************************************************************************************************


@set ImageMagicPath="C:\Program Files\ImageMagick\magick.exe"

@%ImageMagicPath% convert fd_icone16.png fd_icone32.png fd_icone256.png ..\FindDuplicates.ico
